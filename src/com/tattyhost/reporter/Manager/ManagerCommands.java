/**
 * @author TATTYPLAY
 * @date May 21, 2018
 * @created 10:15:43 PM
 * @filename ManageCommands.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import org.bukkit.command.CommandExecutor;

import com.tattyhost.reporter.Reporter;

public class ManagerCommands {
	public static void register(String string, CommandExecutor command) {
		Reporter.getInstance().getCommand(string).setExecutor(command);
	}

}
