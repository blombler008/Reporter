/**
 * @author TATTYPLAY
 * @date May 20, 2018
 * @created 1:19:36 AM
 * @filename Report.java
 * @project Reporter
 * @package com.tattyhost.reporter.report
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.tattyhost.reporter.Reporter;

import net.md_5.bungee.api.ChatColor;

public class ManagerReport {

	private Reporter m = Reporter.getInstance();
	private ManagerConsole cm = m.getManagerConsole();
	private ManagerMySQL mysql = m.getMySQL();
	public String[] sqlResons = { "hacking", "griefing", "spam", "insult" };
	private ResultSet rs;
	private ManagerBan ban = new ManagerBan(mysql);
	public boolean isUserExists(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `reportsall` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			
		}
		return false;
	}

	public void update(UUID uuid, String playerName, String reason, boolean remove) {
		try {
			if (isUserExists(uuid)) {
				if (remove) {
					delete(uuid);
				} else {

					int reportSet = getReportsAll(uuid) + 1;
					int hackingSet = getHackingAll(uuid);
					int griefingSet = getGriefingAll(uuid);
					int spamSet = getSpamAll(uuid);
					int insultSet = getInsultAll(uuid);

					if (reason.equalsIgnoreCase(sqlResons[0].intern())) {
						hackingSet += 1;
					} else if (reason.equalsIgnoreCase(sqlResons[1].intern())) {
						griefingSet += 1;
					} else if (reason.equalsIgnoreCase(sqlResons[2].intern())) {
						spamSet += 1;
					} else if (reason.equalsIgnoreCase(sqlResons[3].intern())) {
						insultSet += 1;
					} else {
						return;
					}
					PreparedStatement ps = mysql.getConnection(false).prepareStatement(
							"UPDATE `reports` SET `reportsall`=?, `hacking` = ?, `griefing`=?, `spam`=?, `insult`=? WHERE  `uuid`=?");

					ps.setInt(1, reportSet);
					ps.setInt(2, hackingSet);
					ps.setInt(3, griefingSet);
					ps.setInt(4, spamSet);
					ps.setInt(5, insultSet);
					ps.setString(6, uuid.toString());

					ps.executeUpdate();
				}

			} else if (!isUserExists(uuid)) {

				int reportSet = 1;
				int hackingSet = 0;
				int griefingSet = 0;
				int spamSet = 0;
				int insultSet = 0;

				if (reason.contains(sqlResons[0].intern())) {
					hackingSet += 1;
				} else if (reason.contains(sqlResons[1].intern())) {
					griefingSet += 1;
				} else if (reason.contains(sqlResons[2].intern())) {
					spamSet += 1;
				} else if (reason.contains(sqlResons[3].intern())) {
					insultSet += 1;
				} else {
					return;
				}
				PreparedStatement ps = mysql.getConnection(false).prepareStatement(
						"INSERT INTO `reports` (`reportsall`, `uuid`,`playername`, `hacking`, `griefing`, `spam`, `insult`) VALUES (?, ?, ?, ?, ?, ?, ?);");

				ps.setInt(1, reportSet);
				ps.setString(2, uuid.toString());
				ps.setString(3, playerName);
				ps.setInt(4, hackingSet);
				ps.setInt(5, griefingSet);
				ps.setInt(6, spamSet);
				ps.setInt(7, insultSet);

				ps.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void delete(UUID uuid) {
		if (isUserExists(uuid)) {
			try {
				PreparedStatement ps = mysql.getConnection(false)
						.prepareStatement("DELETE FROM `reports` WHERE  `uuid`=?");
				ps.setString(1, uuid.toString());
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else
			cm.out(mysql.getPrefix(), "UUID: " + uuid.toString() + " existiert nicht!");
	}

	public void setFinished(UUID uuid, Player ply, long time) {
		
		ban.setUniqueId(uuid);
		ban.removeEntry(true);
		
		if (ban.isUserExist()){
		try {
			ban.getMysql().disconnect(false);
			rs = mysql.getEntryReports(uuid);
			boolean next_ = rs.next();
			if(next_) {
				System.out.println(rs.toString());
				String uuidString = rs.getString("uuid");
				int reportsAll = rs.getInt("reportsall");
				int hacking = rs.getInt("hacking");
				int griefing = rs.getInt("griefing");
				int spam = rs.getInt("spam");
				int insult = rs.getInt("insult");
				
				PreparedStatement ps = mysql.getConnection(false).prepareStatement(
						"UPDATE `reportsfinished` SET `reportsall` = ?, `hacking`=?, `griefing`=?, `spam`=?, `insult`=?, `finishedby`= ?, `time`=? WHERE `uuid` = ?;;");
				ps.setInt(1, reportsAll);
				ps.setInt(2, hacking);
				ps.setInt(3, griefing);
				ps.setInt(4, spam);
				ps.setInt(5, insult);
				ps.setString(6, ply.getName());
				ps.setLong(7, time);
				ps.setString(8, uuidString);
				ps.executeUpdate();
			} else {
				ply.sendMessage(Boolean.toString(next_));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	} else
		if (!ban.isUserExist()) {
			
			try {
				ban.getMysql().disconnect(false);
				rs = mysql.getEntryReports(uuid);
				
				if(rs.next()) {
					System.out.println(rs.toString());
					String uuidString = rs.getString("uuid");
					String playerName = rs.getString("playername");
					int reportsAll = rs.getInt("reportsall");
					int hacking = rs.getInt("hacking");
					int griefing = rs.getInt("griefing");
					int spam = rs.getInt("spam");
					int insult = rs.getInt("insult");
					
					PreparedStatement ps = mysql.getConnection(false).prepareStatement(
							"INSERT INTO `reportsfinished` (`reportsall`,`uuid`,`playername`,`hacking`,`griefing`,`spam`,`insult`,`finishedby`,`time`) VALUES (?,?,?,?,?,?,?,?,?);");
					ps.setInt(1, reportsAll);
					ps.setString(2, uuidString);
					ps.setString(3, playerName);
					ps.setInt(4, hacking);
					ps.setInt(5, griefing);
					ps.setInt(6, spam);
					ps.setInt(7, insult);
					ps.setString(8, ply.getName());
					ps.setLong(9, time);
					ps.executeUpdate();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			update(uuid, null, null, true);
	}

	public Integer getReportsAll(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `reportsall` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("reportsall");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public Integer getGriefingAll(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `griefing` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("griefing");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public Integer getHackingAll(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `hacking` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("hacking");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public Integer getSpamAll(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `spam` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("spam");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public Integer getInsultAll(UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false)
					.prepareStatement("SELECT `insult` FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("insult");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}

	public String getInsultString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.insult");
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}
	
	public String getHackingString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.hacking");
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}
	
	public String getGriefingString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.griefing");
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}
	
	public String getSpamString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.spam");
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}
	
	public String getTitleString(String player) {
		String ret = Reporter.getInstance().getConfig().getString("Report.title");;
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		ret = ret.replaceAll("\\[name\\]", player);
		return ret;
	}

	public String getAllReportString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.allreports");;
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}

	public String getPlayerString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.player");;
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}

	public String getPlayerUUIDString() {
		String ret = Reporter.getInstance().getConfig().getString("Report.uuid");;
		ret = Reporter.translateUmlaute(ret);
		ret = ChatColor.translateAlternateColorCodes('&', ret);
		return ret;
	}

	/**
	 * @return the rs
	 */
	public ResultSet getRs() {
		return rs;
	}

	/**
	 * @param rs the rs to set
	 */
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	
}
