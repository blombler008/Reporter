/**
 * @author TATTYPLAY
 * @date May 21, 2018
 * @created 10:07:26 PM
 * @filename ConsoleManager.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import org.bukkit.ChatColor;

import com.tattyhost.reporter.Reporter;

public class ManagerConsole {
	private static String prefix = Reporter.getPrefixReport();

	public void out(Object message) {
		String convert = "&b" + prefix + "&r" + message.toString() + "&r";
		message = (String) ChatColor.translateAlternateColorCodes('&', convert);
		Reporter.getInstance().getServer().getConsoleSender().sendMessage((String) message);
	}

	public void out(Object prefix, Object message) {
		String convert = "&b" + prefix.toString() + "&r" + message.toString() + "&r";
		message = (String) ChatColor.translateAlternateColorCodes('&', convert);
		Reporter.getInstance().getServer().getConsoleSender().sendMessage((String) message);
	}
}
