/**
 * @author TATTYPLAY
 * @date May 24, 2018
 * @created 7:33:17 PM
 * @filename ManagerMySQL.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.tattyhost.reporter.Reporter;

public class ManagerMySQL {
	
	private String prefix;
	private boolean pref = true;
	private String host;
	private String port;
	private String database;
	private String username;
	private String password;
	
	private ManagerConsole mc = new ManagerConsole();
	
	private Connection con;

	
	
	
	
	
	
	
	
	
	
	
	public ManagerMySQL(String host, String port, String database, String username, String password, boolean pref){
		
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.database = database;
		this.pref = pref;
		
	}
	public ManagerMySQL(String host, String port, String database, String username, String password, boolean pref, String prefix){
		
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.database = database;
		this.pref = pref;
		this.prefix = prefix;
		
	}
	
	
	
	
	
	
	
	
	
	
	public ManagerMySQL connect() {
		return connect(pref);
	}
	
	public ManagerMySQL connect(boolean b) {
		if (!isConnected()) {
			try {
				
				this.con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
				if (b) {
					if (pref) {
						mc.out(this.prefix, "&eVerbindung aufgebaut!");
					}					
				}
				return this;
			} catch (SQLException e) {
				mc.out(Reporter.prefixMySQL,"&4MySQL connection not setup Right");
				
			}
			
		}
		return null;
	}

	
	
	
	
	public boolean isConnected() {
		return (con == null ? false : true);
	}

	
	
	
	
	
	
	
	
	
	public void disconnect() {
		this.disconnect(pref);
	}
	
	public void disconnect(boolean b) {
		if (isConnected()) {
			
			try {
				con.close();
				if (b) {
					if (pref) {
						mc.out(getPrefix(), "&eVerbindung Geschlossen!");
					}
				}
				con = null;
			} catch (SQLException e) {
				
				
				
			}
				
		}
	}
	

	
	
	
	
	
	
	
	
	public Connection getConnection() {
		return this.getConnection(pref);
			
	}
	
	public Connection getConnection(boolean b) {
		refresh(b);
		return this.con;		
	}
	
	
	
	
	
	public ManagerMySQL refresh(){
		return this.refresh(pref);
	}
	
	public ManagerMySQL refresh(boolean b) {
		disconnect(b);
		connect(b);
		return this;
	}
	
	
	
	
	
	
	
	
	
	
	public ResultSet getEntryReports(UUID uuid) {
		
		try {
			PreparedStatement ps = this.getConnection(false).prepareStatement("SELECT * FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			return ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet getEntryFinishedReports(UUID uuid) {
		try {
			PreparedStatement ps = this.getConnection(false).prepareStatement("SELECT * FROM `reportsfinished` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			return ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @return the pref
	 */
	public boolean isPref() {
		return pref;
	}

	/**
	 * @param pref the pref to set
	 */
	public void setPref(boolean pref) {
		this.pref = pref;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return
	 */
	public ManagerConsole getManagerConsole() {
		return mc;
	}
}
