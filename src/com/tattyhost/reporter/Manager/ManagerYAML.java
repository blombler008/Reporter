/**
 * @author TATTYPLAY
 * @date May 21, 2018
 * @created 10:08:06 PM
 * @filename ManagerConfig.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import org.bukkit.configuration.file.FileConfiguration;

import com.tattyhost.reporter.Reporter;

public class ManagerYAML {
	private static FileConfiguration cfg = Reporter.getInstance().getConfig();

	public static boolean getConfigBool(String path) {
		return cfg.getBoolean(path);
	}

	public static String getString(String path) {
		return cfg.getString(path);
	}

	public static int getInt(String path) {
		return cfg.getInt(path);
	}

}
