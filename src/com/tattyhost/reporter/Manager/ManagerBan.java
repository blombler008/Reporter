/**
 * @author TATTYPLAY
 * @date Jun 15, 2018
 * @created 6:46:06 PM
 * @filename ManagerBan.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter.Manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.mysql.jdbc.PreparedStatement;

public class ManagerBan {
	private ManagerMySQL mysql;
	private UUID uniqueId;

	public ManagerBan(ManagerMySQL mysql) {
		this.mysql = mysql;

	}

	/**
	 * @param uniqueId
	 */
	public boolean isUserExist() {
		PreparedStatement ps;
		ResultSet rs;
		try {
			ps = (PreparedStatement) mysql.getConnection(false)
					.prepareStatement("SELECT * FROM `reportsfinished` WHERE `uuid` = ?");
			ps.setString(1, uniqueId.toString());
			rs = ps.executeQuery();
			return rs.next();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean removeEntry() {
		PreparedStatement ps;
		try {
			if (getPlayerInfo().getLong("time") != -1) {
				if (getRemainingTimeInLong() < 0) {
					ps = (PreparedStatement) mysql.getConnection(false)
							.prepareStatement("UPDATE `reportsfinished` SET `banned`='false' WHERE `uuid`=?");
					ps.setString(1, uniqueId.toString());
					ps.executeUpdate();
					return true;

				} else {
					return false;
				}

			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public long getRemainingTimeInLong() throws SQLException {
		long current = System.currentTimeMillis();
		long end = getPlayerInfo().getLong("time");

		return end - current;
	}

	/**
	 * @return the mysql
	 */
	public ManagerMySQL getMysql() {
		return mysql;
	}

	/**
	 * @param mysql
	 *            the mysql to set
	 */
	public ManagerBan setMysql(ManagerMySQL mysql) {
		this.mysql = mysql;
		return this;
	}

	/**
	 * @return the playerInfo
	 */
	public ResultSet getPlayerInfo() {
		PreparedStatement ps;
		ResultSet rs;
		try {
			ps = (PreparedStatement) mysql.getConnection(false)
					.prepareStatement("SELECT * FROM `reportsfinished` WHERE `uuid` = ?");
			ps.setString(1, uniqueId.toString());
			rs = ps.executeQuery();
			rs.next();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public String getRemainingTime() throws SQLException {
		long current = System.currentTimeMillis();
		long end = getPlayerInfo().getLong("time");
		long different = end - current;
		return this.getRemainingTime(different);

	}

	/**
	 * @param different
	 * @return
	 */
	public String getRemainingTime(long different) {

		if (different <= -1) {
			return "Permanent";
		}

		int secounds = 0;
		int minuts = 0;
		int hours = 0;
		int days = 0;
		int weeks = 0;
		int months = 0;

		while (different > 1000) {
			different -= 1000;
			secounds++;
		}
		while (secounds > 60) {
			secounds -= 60;
			minuts++;
		}
		while (minuts > 60) {
			minuts -= 60;
			hours++;
		}
		while (hours > 24) {
			hours -= 24;
			days++;
		}
		while (days > 7) {
			days -= 7;
			weeks++;
		}
		while (weeks > 4) {
			weeks -= 4;
			months++;
		}
		
		String rm = "";
		
		if (months != 0) {
			rm += months + " Month/s(4 Weeks) ";
		}
		if (weeks != 0) {
			rm += weeks + " Week/s ";
		}
		if (days != 0) {
			rm += days + " Day/s ";
		}
		if (hours != 0) {
			rm += hours + " Hour/s ";
		}
		if (minuts != 0) {
			rm += minuts + " Minut/s ";
		}
		if (secounds != 0) {
			rm += secounds + " Secound/s ";
		}
		if (different != 0) {
			// rm += different +"Millis ";
		}
		return rm;
	}

	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	/**
	 * @param uniqueId
	 *            the uniqueId to set
	 */
	public ManagerBan setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
		return this;
	}

	/**
	 * @param force
	 */
	public boolean removeEntry(boolean force) {
		PreparedStatement ps;
		try {
			if(force) {
				
				ps = (PreparedStatement) mysql.getConnection(false)
						.prepareStatement("DELETE FROM `reportsfinished` WHERE `uuid`=?");
				ps.setString(1, uniqueId.toString());
				ps.executeUpdate();
				return true;
				
			} else this.removeEntry();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @return
	 */
	public boolean isBanned() {
		
		try {
			if (getPlayerInfo().getBoolean("banned")) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
