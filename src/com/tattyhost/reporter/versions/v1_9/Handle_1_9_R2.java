/**
 * @author TATTYPLAY
 * @date Jul 9, 2018
 * @created 10:18:29 PM
 * @filename Handle_1_8_R3.java
 * @project ChatColor
 * @package com.tattyhost.chatcolor.listener.chat.v1_8
 * @copyright 2018
 */
package com.tattyhost.reporter.versions.v1_9;

import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.tattyhost.reporter.IVersionHandler;

import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;

/**
 * 
 */
public class Handle_1_9_R2 implements IVersionHandler {

	/*
	 * @see com.tattyhost.chatcolor.listener.chat.FormatChat#sendToPlayer(java.lang.String, java.lang.String, org.bukkit.entity.Player)
	 */
	@Override
	public void sendToPlayers(String message, Player ply, Player sender) {
		
		
		IChatBaseComponent chat = ChatSerializer.a("[\"\"" + message + "]");
		
		PacketPlayOutChat packet = new PacketPlayOutChat(chat);
		((CraftPlayer) ply).getHandle().playerConnection.sendPacket(packet);	
	}

}
