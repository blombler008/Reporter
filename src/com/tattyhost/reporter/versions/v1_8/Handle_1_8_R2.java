/**
 * @author TATTYPLAY
 * @date Jul 11, 2018
 * @created 12:46:48 AM
 * @filename Handler_1_8_R2.java
 * @project ChatColor
 * @package com.tattyhost.chatcolor.listener.chat.v1_8
 * @copyright 2018
 */
package com.tattyhost.reporter.versions.v1_8;

import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.tattyhost.reporter.IVersionHandler;

import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;


/**
 * 
 */
public class Handle_1_8_R2 implements IVersionHandler {

	/*
	 * @see com.tattyhost.chatcolor.listener.chat.FormatChat#sendToPlayer(java.lang.String, java.lang.String, org.bukkit.entity.Player, org.bukkit.entity.Player)
	 */
	@Override
	public void sendToPlayers(String message, Player ply, Player sender) {
		
		
		IChatBaseComponent chat = ChatSerializer.a("[\"\"" + message + "]");
		
		PacketPlayOutChat packet = new PacketPlayOutChat(chat);
		((CraftPlayer) ply).getHandle().playerConnection.sendPacket(packet);	
	}


}
