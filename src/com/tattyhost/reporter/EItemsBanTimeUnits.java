/**
 * @author TATTYPLAY
 * @date Jul 2, 2018
 * @created 11:06:00 PM
 * @filename ItemsBanTimeUnits.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.player.choice.timeunit
 * @copyright 2018
 */
package com.tattyhost.reporter;

public enum EItemsBanTimeUnits {
	
	SECOUNDS(		 1L, "SECOUNDS"	, 0),
	MINUTS	(		60L, "MINUTS"	, 1),
	HOURS	(     3600L, "HOURS"	, 2),
	DAYS	(    86400L, "DAYS"		, 3),
	WEEKS	(	604800L, "WEEKS"	, 4),
	MONTHS	(  2419200L, "MONTHS"	, 5);
	
	private Long time;
	private String name;
	
	
	private EItemsBanTimeUnits(long time, String name, int index) {
		this.time = time;
		this.name = name;
	}

	/**
	 * 
	 * @return the enumname
	 */
	public String toString(){
		return name;
	}
	/**
	 * 
	 * @return the enumtime
	 */
	public long getTime() {
		return time; 
	}
}
