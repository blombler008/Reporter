/**
 * @author TATTYPLAY
 * @date May 22, 2018
 * @created 1:02:22 PM
 * @filename ItemStackE.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackE {

	private String name;
	private Material material;
	private ItemStack itemStack;
	private int slot;
	private int metaDataID;
	private int amount;

	@SuppressWarnings("deprecation")
	public ItemStackE(String name, int material, int metaDataID, int amount, int slot) {
		this(name, Material.getMaterial(material), metaDataID, amount, slot);
		
	}

	public ItemStackE(String name, Material material, int metaDataID, int amount, int slot) {
		this.name = name;
		this.material = material;
		this.slot = (slot) - 1;
		this.metaDataID = metaDataID;
		this.amount = amount;
		
		itemStack = new ItemStack(material, amount, (byte) metaDataID);
		ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setDisplayName(name);
		itemStack.setItemMeta(itemMeta);
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		ItemMeta iMeta = getItemMeta();
		iMeta.setDisplayName(name);
		setItemMeta(iMeta);
	}

	public Material getMaterial() {
		return material;
	}
	
	@Deprecated
	public int getMaterialId() {
		return material.getId();
	}

	public ItemMeta getItemMeta() {
		ItemMeta itemMeta = itemStack.getItemMeta();
		return itemMeta;
	}

	public ItemMeta setItemMeta(ItemMeta itemMeta) {
		itemStack.setItemMeta(itemMeta);
		return this.getItemMeta();
	}

	public ItemStack getItemStack() {
		return itemStack;
	}

	public int getSlot() {
		return slot;
	}

	public int getMetaDataID() {
		return metaDataID;
	}

	public int getAmount() {
		return amount;
	}
	
	
	/**
	 * @param bookAndQuill
	 */
	public ItemStackE setMaterial(Material type) {
		itemStack.setType(type);
		this.material = type;
		return this;
	}

	/**
	 * @param amount
	 */
	public ItemStackE setAmount(int amount) {
		itemStack.setAmount(amount);
		this.amount = amount;
		return this;
	}

	/**
	 * @param lore
	 * @return ItemMeta
	 */
	public ItemMeta setLore(List<String> lore) {
		ItemMeta iMeta = getItemMeta();
		iMeta.setLore(lore);
		this.setItemMeta(iMeta);
		return this.getItemMeta();
	}

}
