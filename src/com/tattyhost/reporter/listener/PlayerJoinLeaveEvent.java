/**
 * @author TATTYPLAY
 * @date Jun 15, 2018
 * @created 12:14:17 PM
 * @filename PlayerJoinLeaveEvent.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener
 * @copyright 2018
 */
package com.tattyhost.reporter.listener;

import java.sql.SQLException;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerBan;
import com.tattyhost.reporter.Manager.ManagerMySQL;

public class PlayerJoinLeaveEvent implements Listener{

			
	
	private Reporter m = Reporter.getInstance();
	private ManagerMySQL mysql = m.getMySQL();
	private ManagerBan ban = new ManagerBan(mysql);
	
	private List<String> msgs = m.getConfig().getStringList("Report.bannmessage");
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		ban.setUniqueId(e.getPlayer().getUniqueId());
		
		if (mysql.isPref()) {
			Player ply = e.getPlayer();
			m.getManagerConsole().out(m.getMySQL().getPrefix(), "&e" + ply.getName() + " Joining! Checking logs");
		}
		
		if (ban.isUserExist()) {
			
			try {
				ban.removeEntry();
				
				if (ban.isBanned()) e.disallow( Result.KICK_BANNED, getKickMessage( e.getPlayer(), ban.getRemainingTime() ) );						
				else e.allow();
				
			} catch (SQLException e1) {} 
			
		} else e.allow();
		
	}
	
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		if (mysql.isPref()){
			Player ply = e.getPlayer();
			m.getManagerConsole().out(m.getMySQL().getPrefix(), "&e" + ply.getName() + " Left! Saving logs");
		}
	}
	
	@EventHandler
	public void onKick(PlayerKickEvent e){
	
	}
	
	
	public String getKickMessage(Player ply, Object time){
		
		String ex = "";
		for (int i = 0; i<msgs.size(); i++) {
			
			String bannmsg = msgs.get(i);
			
			bannmsg = Reporter.translateUmlaute(bannmsg);
			bannmsg = bannmsg.replaceAll("\\[user\\]", ply.getName());
			bannmsg = bannmsg.replaceAll("\\[remain\\]", time.toString());
			bannmsg = ChatColor.translateAlternateColorCodes('&', bannmsg);
			
			ex += "\n" + bannmsg ;
			
		}
		
		return ex;
	}
	
}
