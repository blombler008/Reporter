/**
 * @author TATTYPLAY
 * @date May 22, 2018
 * @created 4:25:51 PM
 * @filename PlayerInventoryClickEvent.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener
 * @copyright 2018
 */
package com.tattyhost.reporter.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.invs.InventoryAdmin;
import com.tattyhost.reporter.inventory.invs.InventoryPlayerChoice;
import com.tattyhost.reporter.inventory.invs.InventoryReports;

public class PlayerInventoryClickEvent implements Listener {

	@EventHandler
	public void onInventory(InventoryClickEvent e) {

		Player ply = (Player) e.getView().getPlayer();
		
		if (ply.hasPermission("reporter.admin")) {
			
			if (e.getInventory().getTitle().contains(CreationInventory.invAdmin.getName())) {
				
				if (e.getInventory().getSize() == CreationInventory.invAdmin.getSlots()) {
					
					new InventoryAdmin(e,ply);
					
				}
				
			} else if (e.getInventory().getTitle().contains(CreationInventory.invReports.getName())) {
				
				if (e.getInventory().getSize() == CreationInventory.invReports.getSlots()) {
					
					new InventoryReports(e, ply);
					
				}
				
			}else if (e.getInventory().getTitle().contains(CreationInventory.invPlayerChoice.getName())) {
				
				if (e.getInventory().getSize() == CreationInventory.invPlayerChoice.getSlots()) {
					
					new InventoryPlayerChoice(e, ply);
					
				}
				
			}
			
		}
		

	}
}
