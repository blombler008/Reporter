/**
 * @author TATTYPLAY
 * @date May 22, 2018
 * @created 6:25:52 PM
 * @filename PlayerBlockPlaceEvent.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener
 * @copyright 2018
 */
package com.tattyhost.reporter.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.EInventoryMode;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.invs.InventoryPlayerChoice;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;
import com.tattyhost.reporter.inventory.player.choice.inventory.InventoryItemsSpring;

public class PlayerClickInteractEvent implements Listener {
	private static FileConfiguration cfg = Reporter.getInstance().getConfig();
	
	@EventHandler
	public void onPlayerBlockPlace(PlayerInteractEvent e) {
		if (e.getPlayer().getItemInHand() != null) {

			ItemStack item = e.getPlayer().getItemInHand();
			if (item.getType() == Material.CHEST || item.getType() == Material.NETHER_STAR || item.getType() == Material.BOOK_AND_QUILL) {
				ItemMeta iMeta = item.getItemMeta();
				if (iMeta.getDisplayName() != null) {

					String iName = iMeta.getDisplayName();

					if (iName.equalsIgnoreCase(CreationInventory.invAdmin.getName())) {
						removeItem(e);
					}
					
					if (iName.equalsIgnoreCase(CreationInventory.invReports.getName())) {
						removeItem(e);
					}
					
					if (iName.equalsIgnoreCase(cfg.getString("Report.homeitemname"))) {
						removeItem(e);
					}
					
				
					if (e.getPlayer().hasPermission("reporter.admin")) {
						try{
							ItemStack reportItems = InventoryItemsSpring.items.get(e.getPlayer());
							if (item.equals(reportItems)){
								InventoryPlayerChoice.mode = EInventoryMode.NONE;
								
								e.setCancelled(true);
								
								for(Player player: Bukkit.getOnlinePlayers()){
									player.showPlayer(e.getPlayer());
								}
								
								e.getPlayer().setCanPickupItems(true);
								e.getPlayer().getInventory().setItemInHand(new ItemStack(Material.AIR));
								new CreationItemsPlayersChoice().createItemToChoice(reportItems, CreationInventory.invPlayerChoice.getInventory(), e.getPlayer());
								e.getPlayer().openInventory(CreationInventory.invPlayerChoice.getInventory());
								InventoryItemsSpring.items.remove(e.getPlayer(),reportItems);
									
							}
								
							
						}catch (Exception es) {
							
						}
						
						
					}
					
					
					
					
				}

			}

		}

	}

	public void removeItem(PlayerInteractEvent e) {

		e.setCancelled(true);
		e.getPlayer().getInventory().setItemInHand(new ItemStack(Material.AIR));

		String playerName = e.getPlayer().getName();

		for (Player player : Bukkit.getOnlinePlayers()) {
			
			if (player.hasPermission("reporter.admin")) {
				
				player.sendMessage(playerName + " tried to open/to place/to use an item of the Admin Report Menu");

			}

		}
	}
}
