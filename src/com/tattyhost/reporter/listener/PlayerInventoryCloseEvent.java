/**
 * @author TATTYPLAY
 * @date May 30, 2018
 * @created 10:02:51 PM
 * @filename PlayerInventoryCloseEvent.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener
 * @copyright 2018
 */
package com.tattyhost.reporter.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import com.tattyhost.reporter.EInventoryMode;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.invs.InventoryAdmin;
import com.tattyhost.reporter.inventory.invs.InventoryPlayerChoice;
import com.tattyhost.reporter.inventory.player.choice.inventory.InventoryItemsBan;

public class PlayerInventoryCloseEvent implements Listener{
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e){
		
		Player ply = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		
		if (ply.hasPermission("reporter.admin")) {
			
			if (inv.getName() != null) {
				
				if(inv.getName().equalsIgnoreCase(CreationInventory.invReports.getName())) {
					
					
					
				}
				
				if(inv.getName().equalsIgnoreCase(CreationInventory.invAdmin.getName())) {

					InventoryAdmin.currpage = 0;
					
				}
				
				if(inv.getName().equalsIgnoreCase(CreationInventory.invPlayerChoice.getName())) {
					
					CreationInventory.invPlayerChoice.getInventory().clear();
					InventoryPlayerChoice.mode = EInventoryMode.NONE;
					InventoryItemsBan.firstinit = true;
					InventoryItemsBan.setPermBann(true);

					InventoryItemsBan.mapOfAmounts.clear();
					InventoryItemsBan.itemStackETListBooks.clear();
					InventoryItemsBan.itemStackETListDown.clear();
					InventoryItemsBan.itemStackETListUp.clear();
				}
				
			}
			
		}
		
	}
	
}
