/**
 * @author TATTYPLAY
 * @date Jun 2, 2018
 * @created 11:52:12 PM
 * @filename InventoryMode.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.invs
 * @copyright 2018
 */
package com.tattyhost.reporter;

public enum EInventoryMode {

	BANN("BANN"),
	REMOVE("REMOVE"),
	SPRING("SPRING"),
	WARN("WARN"), 
	NONE("NONE");
	
	private String string;
	
	
	private EInventoryMode (String string){
		this.string = string;
		
	}
	
	
	public String toString(){
		return string;
	}
}
