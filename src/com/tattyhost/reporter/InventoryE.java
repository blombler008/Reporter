/**
 * @author TATTYPLAY
 * @date May 22, 2018
 * @created 12:47:20 AM
 * @filename ManagerInventory.java
 * @project Reporter
 * @package com.tattyhost.reporter.Manager
 * @copyright 2018
 */
package com.tattyhost.reporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class InventoryE {

	private static List<HashMap<String, Integer>> inventorys = new ArrayList<HashMap<String, Integer>>();
	private static int inventorysIDs = 0;
	private int inventoryID;
	private String name;
	private int slots;
	private Inventory inv;

	public InventoryE(String name, int slots, int inventoryID) {

		HashMap<String, Integer> input = new HashMap<>();
		input.put(name, slots);
		inventorys.add(inventoryID, input);

		this.inventoryID = inventorysIDs;
		inventorysIDs++;

		this.name = ChatColor.translateAlternateColorCodes('&', name);
		this.slots = slots;
		this.inv = Bukkit.createInventory(null, slots, this.name);

	}

	public InventoryE() {

	}
	/////////////////////////////////////////////////////////////////////////////////////
	//
	// STATIC METHODES
	//
	/////////////////////////////////////////////////////////////////////////////////////

	public static HashMap<String, Integer> getInventory(int inventoryID) {
		return getInventorys().get(inventoryID);
	}

	public static List<HashMap<String, Integer>> getInventorys() {
		return inventorys;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	// OPJECT METHODES
	//
	/////////////////////////////////////////////////////////////////////////////////////

	public int getInventoryID() {
		return inventoryID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public int getSlots() {
		return slots;
	}

	public void addItem(ItemStack itackstack, int slot) {
		inv.setItem(slot, itackstack);
	}

	public Inventory getInventory() {
		return inv;
	}

	public void setItem(int slot, ItemStack itemStack) {
		getInventory().setItem(slot, itemStack);
	}


}
