/**
 * @author TATTYPLAY
 * @date Jul 12, 2018
 * @created 8:15:54 PM
 * @filename FormatChat.java
 * @project Reporter
 * @package com.tattyhost.reporter.versions
 * @copyright 2018
 */
package com.tattyhost.reporter;

import org.bukkit.entity.Player;

/**
 * 
 */
public interface IVersionHandler {

	/**
	 * @param format
	 * @param message
	 * @param ply
	 * @param sender
	 */
	void sendToPlayers(String message, Player ply, Player sender);

}
