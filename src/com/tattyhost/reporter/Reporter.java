/**
 * @author TATTYPLAY
 * @date May 19, 2018
 * @created 8:51:38 PM
 * @filename Main.java
 * @project Reporter
 * @package com.tattyhost.reporter.main
 * @copyright 2018
 */
package com.tattyhost.reporter;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.tattyhost.reporter.DSgVOandGDPR.Fileing;
import com.tattyhost.reporter.Manager.ManagerCommands;
import com.tattyhost.reporter.Manager.ManagerConsole;
import com.tattyhost.reporter.Manager.ManagerMySQL;
import com.tattyhost.reporter.Manager.ManagerReport;
import com.tattyhost.reporter.commands.CommandReport;
import com.tattyhost.reporter.commands.CommandReportMenu;
import com.tattyhost.reporter.listener.PlayerClickInteractEvent;
import com.tattyhost.reporter.listener.PlayerInventoryClickEvent;
import com.tattyhost.reporter.listener.PlayerInventoryCloseEvent;
import com.tattyhost.reporter.listener.PlayerJoinLeaveEvent;
import com.tattyhost.reporter.versions.v1_10.Handle_1_10_R1;
import com.tattyhost.reporter.versions.v1_11.Handle_1_11_R1;
import com.tattyhost.reporter.versions.v1_12.Handle_1_12_R1;
import com.tattyhost.reporter.versions.v1_8.Handle_1_8_R1;
import com.tattyhost.reporter.versions.v1_8.Handle_1_8_R2;
import com.tattyhost.reporter.versions.v1_8.Handle_1_8_R3;
import com.tattyhost.reporter.versions.v1_9.Handle_1_9_R1;
import com.tattyhost.reporter.versions.v1_9.Handle_1_9_R2;

public class Reporter extends JavaPlugin {
//	private static boolean pref = true;
//	private static String host = "192.168.0.100";
//	private static String port = "3306";
//	private static String database = "Minecraft";
//	private static String username = "root";
//	private static String password = "1234";
	private boolean pref = getConfig().getBoolean("DataBase.PrefixEnable");
	private String host = getConfig().getString("DataBase.hostname");
	private String port = getConfig().getString("DataBase.port");
	private String database = getConfig().getString("DataBase.database");
	private String username = getConfig().getString("DataBase.username");
	private String password = getConfig().getString("DataBase.password");
	public static String prefixMySQL = "[MySQL] ";
	private static String prefixReport = "[Reporter] ";
	
	private IVersionHandler version;
	
	public boolean starting = true;
	
	private static ManagerMySQL mysql;
	
	private static Reporter instance;

	private ManagerReport rm; 
	private ManagerConsole mc;
	
	private String reportsTable = "CREATE TABLE IF NOT EXISTS `reports` ("
			+ " `reportsall` INT(11) NOT NULL DEFAULT '0',"
			+ "	`uuid` VARCHAR(50) NOT NULL DEFAULT '0',"
			+ "	`playername` VARCHAR(50) NOT NULL DEFAULT '0',"
			+ "	`hacking` INT(11) NOT NULL DEFAULT '0',"
			+ "	`griefing` INT(11) NOT NULL DEFAULT '0',"
			+ "	`spam` INT(11) NOT NULL DEFAULT '0',"
			+ "	`insult` INT(11) NOT NULL DEFAULT '0',"
			+ "	PRIMARY KEY (`uuid`));";

	private String reportsFinishedTable = "CREATE TABLE IF NOT EXISTS `reportsfinished` ("
			+ " `reportsall` INT(11) NOT NULL DEFAULT '0',"
			+ "	`uuid` VARCHAR(50) NOT NULL DEFAULT '0',"
			+ "	`playername` VARCHAR(50) NOT NULL DEFAULT '0',"
			+ "	`hacking` INT(11) NOT NULL DEFAULT '0',"
			+ "	`griefing` INT(11) NOT NULL DEFAULT '0',"
			+ "	`spam` INT(11) NOT NULL DEFAULT '0',"
			+ "	`insult` INT(11) NOT NULL DEFAULT '0',"
			+ "	`finishedby` VARCHAR(50) NOT NULL DEFAULT '0',"
			+ "	`time` BIGINT(20) NOT NULL DEFAULT '-1',"
			+ "	`Created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,"
			+ "	`banned` VARCHAR(50) NOT NULL DEFAULT 'true',"
			+ "	PRIMARY KEY (`uuid`));";
	
	public ManagerConsole getManagerConsole(){
		return mc;
	}
	
	@Override
	public void onEnable() {
		setInstance(this);
		mysql = new ManagerMySQL(host, port, database, username, password, pref, prefixMySQL);
		
		this.mc = mysql.getManagerConsole();		
		this.rm = new ManagerReport();
		
		setVersion();
		
		
		mc.out("&aReporter Plugin Wurde Gestartet!");
		
		saveDefaultConfig();

		if (getConfig().getBoolean("enable")) {
			try {
				new Fileing(getInstance());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			registerMySQLGroup();
			registerEventsGroup();
			registerCommmandGroup();
			
			

		} else {
			onDisable();
			
		}
	}
	
	@Override
	public void onDisable() {
		
		mysql.disconnect();
		setInstance(this);
		mc.out("&cReporter Plugin Wurde Gestoppt!");
		setInstance(null);
		
	}

	private void registerCommmandGroup() {
		if (starting){
			ManagerCommands.register("report", new CommandReport());
			ManagerCommands.register("reportmenu", new CommandReportMenu());
		}
	}

	private void registerEventsGroup() {
		if (starting){
			Bukkit.getPluginManager().registerEvents(new PlayerClickInteractEvent(), this);
			Bukkit.getPluginManager().registerEvents(new PlayerInventoryClickEvent(), this);
			Bukkit.getPluginManager().registerEvents(new PlayerInventoryCloseEvent(), this);
			Bukkit.getPluginManager().registerEvents(new PlayerJoinLeaveEvent(), this);
		}
	}

	private void registerMySQLGroup() {
		if (starting){
			mysql.connect();
			if (!mysql.isConnected()){
				Bukkit.getPluginManager().disablePlugin(this);
				starting = false;
				return;
			}
			try {
				PreparedStatement ps;
				if(mysql.isConnected()){
					ps = mysql.getConnection(false).prepareStatement(reportsTable);
					ps.executeUpdate();
				
					ps = mysql.getConnection(false).prepareStatement(reportsFinishedTable);
					ps.executeUpdate();
					return;
				} else {
					starting = false;
				}
			} catch (SQLException e) {
				starting = false;
			}
			starting = true;
		}
		
	}

	public static String translateUmlaute(String start) {
		start = start.replaceAll("\\[u\\]", "ü");
		start = start.replaceAll("\\[a\\]", "ä");
		start = start.replaceAll("\\[o\\]", "ö");
		start = start.replaceAll("\\[O\\]", "Ö");
		start = start.replaceAll("\\[U\\]", "Ü");
		start = start.replaceAll("\\[A\\]", "Ä");
		start = start.replaceAll("\\[ZS\\]", "ß");
//		start = start.replaceAll("\\[<3swastika<3\\]", "卐");
		return start;
	}

	/**
	 * @return the prefixRprt
	 */
	public static String getPrefixReport() {
		return prefixReport;
	}

	/**
	 * @return the plugin
	 */
	public static Reporter getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	private static void setInstance(Reporter instance) {
		Reporter.instance = instance;
	}

	/**
	 * @return the mysql
	 */
	public ManagerMySQL getMySQL() {
		return mysql;
	}

	/**
	 * @return
	 */
	public ManagerReport getReportManager() {
		return rm;
	}

	/**
	 * @return the version
	 */
	public IVersionHandler getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion() {
		String versionString = Bukkit.getServer().getClass().getPackage().getName();
		versionString = versionString.substring(versionString.lastIndexOf('.'));
		switch (versionString) {
		
		case ".v1_7_R4":
			onDisable();
			break;
		case ".v1_7_R3":
			onDisable();
			break;
		case ".v1_7_R2":
			onDisable();
			break;
		case ".v1_7_R1":
			onDisable();
			break;
			
			
			
			
		
		case ".v1_8_R1":
			version = new Handle_1_8_R1();
			break;
		case ".v1_8_R2":
			version = new Handle_1_8_R2();
			break;
		case ".v1_8_R3":
			version = new Handle_1_8_R3();
			break;
			
			
			
			
		
		case ".v1_9_R1":
			version = new Handle_1_9_R1();
			break;
		case ".v1_9_R2":
			version = new Handle_1_9_R2();
			break;
		
			
			
			
			
		case ".v1_10_R1":
			version = new Handle_1_10_R1();
			break;
		
			
			
			
			
		case ".v1_11_R1":
			version = new Handle_1_11_R1();
			break;
		
			
			
			
			
		case ".v1_12_R1":
			version = new Handle_1_12_R1();
			break;
		
		default:
			System.out.println(versionString);
			onDisable();
			break;
		}
	}
	
}
