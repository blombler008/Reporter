/**
 * @author TATTYPLAY
 * @date May 23, 2018
 * @created 8:59:19 PM
 * @filename CreationInventory.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory;

import com.tattyhost.reporter.InventoryE;

public class CreationInventory {
	public static InventoryE invAdmin 				= new InventoryE("&4&lAdmin Report menu"	, 9 * 6		, 0);
	public static InventoryE invReports 			= new InventoryE("&4&lReports Overview"		, 9 * 6		, 1);
	public static InventoryE invPlayerChoice	 	= new InventoryE("&6&lReport Choices"		, 9 * 3		, 2);
}
