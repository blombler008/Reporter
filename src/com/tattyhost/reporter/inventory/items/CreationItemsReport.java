/**
 * @author TATTYPLAY
 * @date May 26, 2018
 * @created 1:22:05 AM
 * @filename CreationItemsReport.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.items;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.ItemStackE;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerMySQL;
import com.tattyhost.reporter.Manager.ManagerReport;

public class CreationItemsReport {

//	public static String[] playernames;
//	public static String[] uuids;
//	public static int[] reportsall;
//	public static int[] hacking;
//	public static int[] griefing;
//	public static int[] spam;
//	public static int[] insult;
	private Reporter ma = Reporter.getInstance();
	private ManagerMySQL mysql = ma.getMySQL();
	private ManagerReport mr = new ManagerReport();
	
	private static List<Object[]> itemSetOut = new ArrayList<Object[]>();
	
	private static List<ItemStackE> items = new ArrayList<ItemStackE>();

	private List<Map<String, Object>> resultSetToList() {
		try {
			
			PreparedStatement ps = mysql.getConnection(false).prepareStatement("SELECT * FROM `reports`;");
			ResultSet rs = ps.executeQuery();
			
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();

			List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();

			while (rs.next()) {

				Map<String, Object> row = new HashMap<String, Object>(columns);

				for (int i = 1; i <= columns; ++i) {
					row.put(md.getColumnName(i), rs.getObject(i));
				}
				
				rows.add(row);
			}
			return rows;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}
	
	public ItemStack createReportBook(boolean bool, UUID uuid) {
		try {
			PreparedStatement ps = mysql.getConnection(false).prepareStatement("SELECT * FROM `reports` WHERE `uuid` = ?");
			ps.setString(1, uuid.toString());
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			if (rs != null) {
				int insultNumber = (int) rs.getInt("insult");
				int hackingNumber = (int) rs.getInt("hacking");
				int griefingumber = (int) rs.getInt("griefing");
				int spamNumber = (int) rs.getInt("spam");
				int reportNumber = (int) rs.getInt("reportsall");
				
				String playerName = (String) rs.getString("playername");
				String UUIDPlayer = (String) rs.getString("uuid");
	
				String insultString 	= mr.getInsultString();
				String hackingString 	= mr.getHackingString();
				String griefingString 	= mr.getGriefingString();
				String spamString 		= mr.getSpamString();
				String allreports 		= mr.getAllReportString();
				String player 			= mr.getPlayerString();
				String playerUUID 		= mr.getPlayerUUIDString();
				
				String spacer = ma.getConfig().getString("Report.spacer");
						
				String title = mr.getTitleString(playerName);	
	
				int material = 340;	
				
				ItemStackE reportBook = new ItemStackE(title, material, 0, 1, 0);
				ItemMeta iMeta = reportBook.getItemMeta();
	
				List<String> lore = new ArrayList<String>();
				
				lore.add(insultString 	+ spacer + insultNumber);
				lore.add(hackingString 	+ spacer + hackingNumber);
				lore.add(griefingString + spacer +  griefingumber);
				lore.add(spamString 	+ spacer +  spamNumber);
				lore.add(allreports 	+ spacer +  reportNumber );
				lore.add(player 		+ spacer +  playerName);
				lore.add(playerUUID 	+ spacer +  UUIDPlayer );
				
				iMeta.setLore(lore);
				reportBook.setItemMeta(iMeta);
				
				return reportBook.getItemStack();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void createReportBooks(int page, Inventory inventory, boolean bool) {

		
		
		List<Map<String, Object>> resultSet = resultSetToList();
		
		
		int setget = resultSet.size();
		
		List<Object[]> itemSet = new ArrayList<Object[]>();
		
		int run = 0;

		for (int m = 0; m < setget;) {
			m++;

			int insultNumber = (int) resultSet.get(run).get("insult");
			int hackingNumber = (int) resultSet.get(run).get("hacking");
			int griefingumber = (int) resultSet.get(run).get("griefing");
			int spamNumber = (int) resultSet.get(run).get("spam");
			int reportNumber = (int) resultSet.get(run).get("reportsall");
			
			String playerName = (String) resultSet.get(run).get("playername");
			String UUIDPlayer = (String) resultSet.get(run).get("uuid");

			String insultString 	= mr.getInsultString();
			String hackingString 	= mr.getHackingString();
			String griefingString 	= mr.getGriefingString();
			String spamString 		= mr.getSpamString();
			String allreports 		= mr.getAllReportString();
			String player 			= mr.getPlayerString();
			String playerUUID 		= mr.getPlayerUUIDString();
			
			String  spacer = ma.getConfig().getString("Report.spacer");
					
			String title = mr.getTitleString(playerName);	

			int material = 340;	
			
			ItemStackE reportBooks = new ItemStackE(title, material, 0, 1, m);
			ItemMeta iMeta = reportBooks.getItemMeta();

			List<String> lore = new ArrayList<String>();
			lore.add(insultString 	+ spacer + insultNumber);
			lore.add(hackingString 	+ spacer + hackingNumber);
			lore.add(griefingString + spacer +  griefingumber);
			lore.add(spamString 	+ spacer +  spamNumber);
			lore.add(allreports 	+ spacer +  reportNumber );
			lore.add(player 		+ spacer +  playerName);
			lore.add(playerUUID 	+ spacer +  UUIDPlayer );
			
			iMeta.setLore(lore);
			reportBooks.setItemMeta(iMeta);

			items.add(reportBooks);
			
			if (m == 45) {
				itemSet.add(items.toArray());
				itemSetOut.add(items.toArray());
				setget -= 45;
				m = 0;
				items.clear();
			}
			run++;

		}
		itemSet.add(items.toArray());
		itemSetOut.add(items.toArray());
		
		items.clear();
		
		if (bool) {
			addItemsToPage(page, inventory, itemSet );
		}
	}
	
	public void addItemsToPage(int page, Inventory inv, List<Object[]> list) {
		
		Object[] itemsArray = list.get(page);
		
		for (int i=0; i< itemsArray.length; i++) {
			try {
				
				ItemStackE item = (ItemStackE) itemsArray[i];
				inv.setItem(item.getSlot(), item.getItemStack());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}


	public ItemStack getBook(int slot, int page) {
		
		createReportBooks( 0, null, false );
		
		Object[] itemsArray = itemSetOut.get(page);
		
		ItemStackE item = (ItemStackE) itemsArray[slot];
		
		return item.getItemStack();
		
	}

}
