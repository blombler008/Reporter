/**
 * @author TATTYPLAY
 * @date May 30, 2018
 * @created 10:39:21 PM
 * @filename CreationItemsPlayersChoice.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.items
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.items;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.tattyhost.reporter.ItemStackE;
import com.tattyhost.reporter.Reporter;

public class CreationItemsPlayersChoice {
	private static FileConfiguration cfg 		= Reporter.getInstance().getConfig();
	
	private static String skullText;
	private static String removeWool;
	private static String banWool;
	private static String warnWool;
	private static String springWool;
	
	public static ItemStackE itemRemoveWool 	= new ItemStackE(""	, Material.WOOL			, 5		, 1		, (9 * 3) / 2 			);
	public static ItemStackE itemBanWool 		= new ItemStackE(""	, Material.WOOL			, 14	, 1		, ((9 * 3) / 2) + 4 	);
	public static ItemStackE itemWarnWool 		= new ItemStackE(""	, Material.WOOL			, 4		, 1		, ((9 * 3) / 2) + 2		);
	public static ItemStackE itemSpringWool		= new ItemStackE(""	, Material.WOOL			, 9		, 1		, ((9 * 3) / 2) - 2		);
			
	public static ItemStackE itemSkull 			= new ItemStackE("" , Material.SKULL_ITEM	, 3		, 1		, (9 / 2) + 1			);
	public static ItemStackE itemReport			= new ItemStackE("" , Material.BOOK			, 0 	, 1		, (( 9 * 3) / 2) + 1 	);
	
	public static CreateHomeStar star 			= new CreateHomeStar((9 * 2) + 5);
	
	public void createItemToChoice(ItemStack report, Inventory inv, Player ply){
		try {
			initStrings();
			
			renameWools();
			
			modifySkull(report);
			
			
			
			ItemMeta reportMeta = report.getItemMeta();
			itemReport.setItemMeta(reportMeta);
			itemReport.setMaterial(Material.BOOK_AND_QUILL);
			
			
			inv.setItem(itemRemoveWool.getSlot()	, itemRemoveWool.getItemStack());
			inv.setItem(itemSpringWool.getSlot()	, itemSpringWool.getItemStack());
			inv.setItem(itemWarnWool.getSlot()		, itemWarnWool.getItemStack());
			inv.setItem(itemBanWool.getSlot()		, itemBanWool.getItemStack());
			inv.setItem(itemReport.getSlot()		, itemReport.getItemStack());
			inv.setItem(itemSkull.getSlot()			, itemSkull.getItemStack());
			inv.setItem(star.getSlot()				, star.getItem());
			ply.updateInventory();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void initStrings() {
		
		skullText 	= cfg.getString("Report.skulltext");
		removeWool 	= cfg.getString("Report.woolRemove");
		banWool 	= cfg.getString("Report.woolBan");
		warnWool 	= cfg.getString("Report.woolWarn");
		springWool 	= cfg.getString("Report.woolSpring");
		
	}


	private void modifySkull(ItemStack report) {
		
		skullText = cfg.getString("Report.skulltext");
		
		List<String> reportLore = report.getItemMeta().getLore();
		
		int playerNameLength = cfg.getString("Report.player").length() + cfg.getString("Report.spacer").length();
		
		String playerName = reportLore.get(5);
		playerName = playerName.substring(playerNameLength);

		skullText = ChatColor.translateAlternateColorCodes('&', skullText);
		skullText = Reporter.translateUmlaute(skullText);
		skullText = skullText.replaceAll("\\[name\\]", playerName);
		
		SkullMeta iMeta = (SkullMeta) itemSkull.getItemMeta();
		
		iMeta.setDisplayName(skullText);
		iMeta.setOwner(playerName);
		
		itemSkull.setItemMeta(iMeta);
	}

	private void renameWools() {
		
		removeWool = ChatColor.translateAlternateColorCodes('&', removeWool);
		removeWool = Reporter.translateUmlaute(removeWool);
		itemRemoveWool.setName(removeWool);
		
		{
			banWool = ChatColor.translateAlternateColorCodes('&', banWool);
			banWool = Reporter.translateUmlaute(banWool);
			setLore(itemBanWool);
			itemBanWool.setName(banWool);
		}
		
		warnWool = ChatColor.translateAlternateColorCodes('&', warnWool);
		warnWool = Reporter.translateUmlaute(warnWool);
		itemWarnWool.setName(warnWool);
		
		springWool = ChatColor.translateAlternateColorCodes('&', springWool);
		springWool = Reporter.translateUmlaute(springWool);
		itemSpringWool.setName(springWool);
		
	}

	/**
	 * @param itemBanWool
	 */
	private void setLore(ItemStackE itemBanWool) {
		List<String> lore = new ArrayList<String>();
		lore.add("�7ALPHA Things (Could not Work / Fixing soon!)");
		lore.add("�7 - Time to ban the player in item form (you can see)");
		lore.add(" �7 �7 �7 could be a buggy time for the players ban");
		lore.add(" �7 �7 �7 means you decide your self!");
		lore.add("�7 - If 2 Admins use 2 diffrent reports, the menu gets stuck (Fixing Soon!)");
		lore.add("�7ALPHA Things (Could not Work / Fixing soon!)");
		lore.add("�7 - Not working now!");
		lore.add("�5Please note to use the manuel ban (Chat)");
		itemBanWool.setItemMeta(itemBanWool.setLore(lore));
	}	
	
}
