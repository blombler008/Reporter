/**
 * @author TATTYPLAY
 * @date May 31, 2018
 * @created 10:01:38 PM
 * @filename CreateHomeStar.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.items
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.items;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.ItemStackE;
import com.tattyhost.reporter.Reporter;

public class CreateHomeStar {
	private static FileConfiguration cfg = Reporter.getInstance().getConfig();
	
	private ItemStackE itemHomeStar;
	
	public CreateHomeStar(int slot) {
		
		String starName = cfg.getString("Report.homeitemname");
		itemHomeStar = new ItemStackE("", Material.NETHER_STAR, 0, 1, slot);
		
		List<String> itemLore = cfg.getStringList("Report.homeitemlore");
		
		ItemMeta iMeta = itemHomeStar.getItemMeta();

		starName = ChatColor.translateAlternateColorCodes('&', starName);
		starName = Reporter.translateUmlaute(starName);
		
		iMeta.setDisplayName(starName);
		
//		itemLore.add("�eClicke um Zurueck");
//		itemLore.add("�eZum Hauptmenu zu kommen");

		for (int i=0; i<itemLore.size(); i++){
			String s = (String) itemLore.get(i);
			s = Reporter.translateUmlaute(s);
			s = ChatColor.translateAlternateColorCodes('&', s);
			itemLore.remove(i);
			itemLore.add(i, s);
		}
		
		iMeta.setLore(itemLore);

		itemHomeStar.setItemMeta(iMeta);
		
	}
	public int getSlot(){
		return itemHomeStar.getSlot();
	}
	public ItemStack getItem(){
		return itemHomeStar.getItemStack();
	}
}
