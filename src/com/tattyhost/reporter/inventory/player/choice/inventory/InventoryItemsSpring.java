/**
 * @author TATTYPLAY
 * @date Jun 7, 2018
 * @created 6:44:19 PM
 * @filename InventoryItemsSpring.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.player.choice.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.player.choice.inventory;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.tattyhost.reporter.EInventoryMode;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.inventory.invs.InventoryPlayerChoice;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;


public class InventoryItemsSpring {
	public static HashMap<Player,ItemStack> items = new HashMap<Player,ItemStack>();
		
	/**
	 * @param e
	 * @param ply
	 */
	public InventoryItemsSpring(InventoryClickEvent e, Player ply) {
		if (ply.hasPermission("reporter.admin")) {
			
			Inventory inv = e.getInventory();
			
			ItemStack item = inv.getItem(CreationItemsPlayersChoice.itemReport.getSlot());
			ItemStack[] playerInventoryItem = ply.getInventory().getContents();
			
			for(int i = 0; i<playerInventoryItem.length;i++){
				
				if(playerInventoryItem[i]!= null){
					if(playerInventoryItem[i].equals(item)){
						String cfgReportresived = Reporter.getInstance().getConfig().getString("Report.resivedbookalready");
						if (!cfgReportresived.equals(" ")) {
							cfgReportresived = Reporter.translateUmlaute(cfgReportresived);
							cfgReportresived = ChatColor.translateAlternateColorCodes('&', cfgReportresived);
							
							ply.sendMessage(cfgReportresived);
						}
						return;
						
					}
				}
			}
			
			
			List<String> itemLore = item.getItemMeta().getLore();
			
			for(String itemLoreString : itemLore){
			
					String playerName = Reporter.getInstance().getConfig().getString("Report.player");
					String spacer = Reporter.getInstance().getConfig().getString("Report.spacer");
					
					itemLoreString = itemLoreString.substring(playerName.length()+spacer.length());
					if(Bukkit.getPlayer(itemLoreString) != null){
						if(items.get(ply) == null){
							items.put(ply, item);
							ply.closeInventory();
							ply.getInventory().addItem(item);
							ply.teleport(Bukkit.getPlayer(itemLoreString).getLocation());
							ply.setGameMode(GameMode.CREATIVE);
							ply.setCanPickupItems(false);
							for(Player player: Bukkit.getOnlinePlayers()){
								
								if(player.hasPermission("reporter.admin") == false || player.getName().equals(itemLoreString)){
									
									player.hidePlayer(ply);
									
								}
								
							}
							String cfgReportresived = Reporter.getInstance().getConfig().getString("Report.resivedreport");
							if (!cfgReportresived.equals(" ")) {
								cfgReportresived = Reporter.translateUmlaute(cfgReportresived);
								cfgReportresived = ChatColor.translateAlternateColorCodes('&', cfgReportresived);
								
								ply.sendMessage(cfgReportresived);
							}
						}else {
							
							InventoryPlayerChoice.mode = EInventoryMode.NONE;
							String cfgReportresived = Reporter.getInstance().getConfig().getString("Report.resivedbookalready");
							if (!cfgReportresived.equals(" ")) {
								cfgReportresived = Reporter.translateUmlaute(cfgReportresived);
								cfgReportresived = ChatColor.translateAlternateColorCodes('&', cfgReportresived);
								
								ply.sendMessage(cfgReportresived);
							}
							
						}
					}
			
				
			}
			e.setCancelled(true);
			
			
			
		}
		
		
	}
	
}
