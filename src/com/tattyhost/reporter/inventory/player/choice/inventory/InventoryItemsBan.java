/**
 * @author TATTYPLAY
 * @date May 31, 2018
 * @created 9:58:57 PM
 * @filename InventoryItemsBan.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.player.choice.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.player.choice.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.EItemsBanTimeUnits;
import com.tattyhost.reporter.ItemStackE;
import com.tattyhost.reporter.ItemStackET;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerReport;
import com.tattyhost.reporter.inventory.items.CreateHomeStar;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;
import com.tattyhost.reporter.listener.PlayerJoinLeaveEvent;

public class InventoryItemsBan {

	private int uuidLength;
	private int insultLength;
	private int hackingLength;
	private int spamLength;
	private int griefingLength;
	private int playerLength;
	private int allLength;
	private int spacerLength;

	private ItemStackE permanentBan;
	private ItemStackE submit;
	private ItemStackE back;
	
	private static ItemStack reportItem;
	private CreateHomeStar star;

	public static List<ItemStackET> itemStackETListBooks = new ArrayList<ItemStackET>();
	public static List<ItemStackET> itemStackETListUp = new ArrayList<ItemStackET>();
	public static List<ItemStackET> itemStackETListDown = new ArrayList<ItemStackET>();
	
	private List<EItemsBanTimeUnits> ItemsBanTimeUnitsList = new ArrayList<EItemsBanTimeUnits>();
	
	public static HashMap<ItemStackET, Integer> mapOfAmounts = new HashMap<ItemStackET, Integer>();
	
	public static boolean firstinit = true;
	public static boolean permBan = false;
	public UUID uuid;
	
	private FileConfiguration cfg = Reporter.getInstance().getConfig();
	
	private long time;
	
	
	
	public InventoryItemsBan(InventoryClickEvent e, Player ply) {

		List<String> bookNames = new ArrayList<String>();
		
		ItemsBanTimeUnitsList.add(0, EItemsBanTimeUnits.SECOUNDS);
		ItemsBanTimeUnitsList.add(1, EItemsBanTimeUnits.MINUTS);
		ItemsBanTimeUnitsList.add(2, EItemsBanTimeUnits.HOURS);
		ItemsBanTimeUnitsList.add(3, EItemsBanTimeUnits.DAYS);
		ItemsBanTimeUnitsList.add(4, EItemsBanTimeUnits.WEEKS);
		ItemsBanTimeUnitsList.add(5, EItemsBanTimeUnits.MONTHS);
		
		bookNames.add(0, ChatColor.YELLOW + "Sekunden");
		bookNames.add(1, ChatColor.YELLOW + "Minuten");
		bookNames.add(2, ChatColor.YELLOW + "Stunden");
		bookNames.add(3, ChatColor.YELLOW + "Tage");
		bookNames.add(4, ChatColor.YELLOW + "Wochen");
		bookNames.add(5, ChatColor.YELLOW + "Monate");

		String uptitle 			= getConfigName("Report.uptitle");
		String downtitle 		= getConfigName("Report.downtitle");
		
		if (ply.hasPermission("reporter.admin")) {

			try {
				Inventory inv = e.getInventory();
				if (firstinit) {
					
					firstinit = false;
					time = 0;
					permBan = false;
					mapOfAmounts.clear();
					itemStackETListBooks.clear();
					itemStackETListDown.clear();
					itemStackETListUp.clear();
					reportItem = inv.getItem(CreationItemsPlayersChoice.itemReport.getSlot());
					
					for (int i = 0; i < 6; i++) {
						
						itemStackETListBooks.add(i, new ItemStackET(bookNames.get(i), Material.BOOK, 0, 0, 12, ItemsBanTimeUnitsList.get(i)));
						itemStackETListUp.add(i, new ItemStackET(itemStackETListBooks.get(i).getName() + uptitle, Material.INK_SACK, 10, 1, 3, itemStackETListBooks.get(i).getTimeUnit()));
						itemStackETListDown.add(i, new ItemStackET(itemStackETListBooks.get(i).getName() + downtitle, Material.INK_SACK, 8, 1, 21, itemStackETListBooks.get(i).getTimeUnit()));
						mapOfAmounts.put(itemStackETListBooks.get(i), 0);
						
					}
				}
				
				
				resetlengths();
				initLengths();
				
				setBanItems(inv, ply, reportItem, e, e.getCurrentItem(), bookNames);
				
			} catch (Exception e2) {
				e2.printStackTrace();
			} finally {
				e.setCancelled(true);				
			}
			
		}

	}


	/**
	 * @param inv
	 * @param e
	 * @param ply
	 * @param e
	 * @param itemClicked 
	 * @param bookNames 
	 */
//	private void setBanItems(Inventory inv, Player ply, ItemStack report, InventoryClickEvent e, ItemStack itemClicked) {
//
//		inv.clear();
//		String permTitleOn = getConfigName("Report.permtitleon");
//		String permTitleOff = getConfigName("Report.permtitleoff");
//		String submittitle = getConfigName("Report.submit");
//		String backtitle = getConfigName("Report.back");
//		String uptitle = getConfigName("Report.uptitle");
//		String downtitle = getConfigName("Report.downtitle");
//
//		permanentBan = new ItemStackE(permTitleOn, Material.BARRIER, 0, 1, 2);
//		submit = new ItemStackE(submittitle, Material.BARRIER, 0, 1, 3);
//		back = new ItemStackE(backtitle, Material.BOOK_AND_QUILL, 0, 1, 21);
////		upItem = new ItemStackE(uptitle, Material.INK_SACK, 10, 1, 9);
////		downItem = new ItemStackE(downtitle, Material.INK_SACK, 8, 1, 27);
//
//		star = new CreateHomeStar(20);
//
//		//List<String> amounts = cfg.getStringList("Report.booktimes.books");
//		List<String> amounts = new ArrayList<String>();
//		amounts.add("Sekunden");
//		amounts.add("Minuten");
//		amounts.add("Stunden");
//		amounts.add("Tage");
//		amounts.add("Wochen");
//		amounts.add("Monate");
//		
//		
//		
//		if (itemClicked.getType() != Material.AIR) {
//			if (itemClicked.getType() == Material.BARRIER) {
//				if (e.getSlot() == permanentBan.getSlot()) {
//					System.out.print("change permBan form " + permBan);
//					setPermBann(permBan ? false : true);
//					System.out.println(" to " + permBan);
//				}
//			}
//		}
//
//		permanentBan.setName(permBan ? permTitleOff : permTitleOn);
//		if (permBan) {
//			
//			if (amounts.size() <= 6) {
//				for (int i = 0; i < amounts.size(); i++) {
//					String currStr = amounts.get(i);
//	
//					currStr = Main.translateUmlaute(currStr);
//					currStr = ChatColor.translateAlternateColorCodes('&', currStr);
//	
////					inv.setItem(upItem.getSlot() - i, upItem.getItemStack());
////					inv.setItem(downItem.getSlot() - i, downItem.getItemStack());
//					
//					if (upItemList.size()<=i){
////						upItemList.add(i, new ItemStackE(
////								upItem.getName(),
////								upItem.getMaterial(),
////								upItem.getMetaDataID(),
////								upItem.getAmount(),
////								upItem.getSlot() - i )
////						);
//					} 
//					if (downItemList.size()<=i){
////						downItemList.add(i, new ItemStackE(
////								downItem.getName(),
////								downItem.getMaterial(),
////								downItem.getMetaDataID(),
////								downItem.getAmount(),
////								downItem.getSlot() - i )
////						);
//					} 
//
//					try {
//						int amount = amountStup.get(i);
//						
//						if (itemClicked.getType() != Material.AIR) {
//							if (itemClicked.getType() == Material.INK_SACK) {
//								
////								if (e.getSlot() == (upItem.getSlot() - i)) {
////									if (amount == 60) {
////										amount = 59;
////										
////									}
////									amount += 1;
////									
////								} else if (e.getSlot() == (downItem.getSlot() - i)) {
////									if (amount == 0) {
////										amount = 1;
////									}
////									amount -= 1;
////									
////								}	
//
//								
//							}
//						}
//						amountStup.set(i, amount);
////						currItem = new ItemStackE(
////								(amount >= 1) ? (currStr + " x" + amount) : (currStr + " x0"),
////								Material.BOOK, 
////								0, 
////								(amount == 0) ? (amount+1) : (amount), 
////								18 - i
////						);
//						
//					} catch (IndexOutOfBoundsException e2) {
//						amountStup.add(0);
////						currItem = new ItemStackE(currStr + " x0", Material.BOOK, 0, 1, 18 - i);
//					}
//	
////					inv.setItem(currItem.getSlot(), currItem.getItemStack());
//				}
//			}
//			
//
//		} 
//
//		inv.setItem(submit.getSlot(), submit.getItemStack());
//		inv.setItem(back.getSlot(), back.getItemStack());
//		inv.setItem(star.getSlot(), star.getItem());
//		inv.setItem(permanentBan.getSlot(), permanentBan.getItemStack());
//
//		submitBan(itemClicked, report, ply, e);
//	}

	
	private void setBanItems(Inventory inv, Player ply, ItemStack report, InventoryClickEvent e, ItemStack itemClicked, List<String> bookNames) {

		inv.clear();
		
		String permTitleOn 		= getConfigName("Report.permtitleon");
		String permTitleOff 	= getConfigName("Report.permtitleoff");
		String submittitle 		= getConfigName("Report.submit");
		String backtitle 		= getConfigName("Report.back");
		
		permanentBan = new ItemStackE(permTitleOn, Material.BARRIER, 0, 1, 2);
		submit = new ItemStackE(submittitle, Material.BARRIER, 0, 1, 3);
		back = new ItemStackE(backtitle, Material.BOOK_AND_QUILL, 0, 1, 21);

		{
			List<String> lorePerm = new ArrayList<String>();
			lorePerm.add("�7ALPHA Things (Could not Work / Fixing soon!)");
			lorePerm.add("�7 - Time to ban the player in item form (you can see)");
			lorePerm.add(" �7 �7 �7 could be a buggy time for the players ban");
			lorePerm.add(" �7 �7 �7 means you decide your self!");
			lorePerm.add("�7 - If 2 Admins use 2 diffrent reports, the menu gets stuck (Fixing Soon!)");
			
			ItemMeta permanentBanMeta = permanentBan.getItemMeta();
			permanentBanMeta.setLore(lorePerm);
			permanentBan.setItemMeta(permanentBanMeta);
		}
		{
			List<String> loreSubmit = new ArrayList<String>();
			loreSubmit.add("�7ALPHA Things (Could not Work / Fixing soon!)");
			loreSubmit.add("�7 - Time to ban the player in item form (you can see)");
			loreSubmit.add(" �7 �7 �7 could be a buggy time for the players ban");
			loreSubmit.add(" �7 �7 �7 means you decide your self!");
			loreSubmit.add("�7 - If 2 Admins use 2 diffrent reports, the menu gets stuck (Fixing Soon!)");
			
			ItemMeta submitMeta = submit.getItemMeta();
			submitMeta.setLore(loreSubmit);
			submit.setItemMeta(submitMeta);
		}
		{
			List<String> loreBack = new ArrayList<String>();
			loreBack.add("�7ALPHA Things (Could not Work / Fixing soon!)");
			loreBack.add("�7 - Not working now!");
			
			ItemMeta backMeta = back.getItemMeta();
			backMeta.setLore(loreBack);
			back.setItemMeta(backMeta);
		}
		
		
		
		star = new CreateHomeStar(20);

		if (itemClicked.getType() != Material.AIR) {
			if (itemClicked.getType() == Material.BARRIER) {
				if (e.getSlot() == permanentBan.getSlot()) {
					setPermBann(permBan ? false : true);
				}
			}
		}

		permanentBan.setName(permBan ? permTitleOff : permTitleOn);
		
		if (permBan) {
			
			if (e.getSlot() <= 8 && e.getSlot() >= 3) {
				actionUpKlicked(e, ply, itemStackETListUp, itemStackETListBooks);
			} else if (e.getSlot() >= 21 && e.getSlot() <= 28) {
				actionDownKlicked(e, ply, itemStackETListDown, itemStackETListBooks);
			}

			showItems(e, itemStackETListBooks, inv);
			showItems(e, itemStackETListDown, inv);
			showItems(e, itemStackETListUp, inv);
			
			

		} 

		inv.setItem(submit.getSlot(), submit.getItemStack());
		inv.setItem(back.getSlot(), back.getItemStack());
		inv.setItem(star.getSlot(), star.getItem());
		inv.setItem(permanentBan.getSlot(), permanentBan.getItemStack());

		submitBan(itemClicked, report, ply, e);
	}
	
	
	
	/**
	 * @param e
	 * @param ply
	 * @param itemStackETListUp
	 * @param itemStackETListBooks
	 */
	private void actionDownKlicked(InventoryClickEvent e, Player ply, List<ItemStackET> itemStackETListUp, List<ItemStackET> itemStackETListBooks) {
		int index = 0;
		switch ( e.getSlot()) {
		case 21:
			index = 5;
			break;
		case 22:
			index = 4;
			break;
		case 23:
			index = 3;
			break;
		case 24:
			index = 2;
			break;
		case 25:
			index = 1;
			break;
		case 26:
			index = 0;
			break;
		}
		try {
			
			ItemStackET item = itemStackETListBooks.get(index);
			int amount = mapOfAmounts.get(item);
			if (amount == 0) {
				amount = 1;
			}
			item.setAmount(amount - 1);			
			mapOfAmounts.remove(item);
			mapOfAmounts.put(item, item.getAmount());
		} catch (IndexOutOfBoundsException e2) {
			return;
		}
	}
	// 

	/**
	 * @param e
	 * @param ply
	 * @param itemStackETListUp
	 * @param itemStackETListBooks
	 */
	private void actionUpKlicked(InventoryClickEvent e, Player ply, List<ItemStackET> itemStackETListUp, List<ItemStackET> itemStackETListBooks) {
		int index = 0;
		switch ( e.getSlot()) {
		case 3:
			index = 5;
			break;
		case 4:
			index = 4;
			break;
		case 5:
			index = 3;
			break;
		case 6:
			index = 2;
			break;
		case 7:
			index = 1;
			break;
		case 8:
			index = 0;
			break;
		}
		try {
			ItemStackET item = itemStackETListBooks.get(index);
			int amount = mapOfAmounts.get(item);
			if (amount == 60) {
				amount = 59;
			}
			item.setAmount(amount + 1);	
			mapOfAmounts.remove(item);
			mapOfAmounts.put(item, item.getAmount());
		} catch (IndexOutOfBoundsException e2) {
			return;
		}
		
		
	}


	/**
	 * @param e
	 * @param itemStackETList
	 * @param inv 
	 */
	private void showItems(InventoryClickEvent e, List<ItemStackET> itemStackETList, Inventory inv) {

		int run = 6; 
		for(ItemStackET item : itemStackETList) {
			inv.setItem(item.getSlot() + run,item.getItemStack());				
			run --;
		}
	}


	/**
	 * @param e 
	 * 
	 */
	private void submitBan(ItemStack itemClicked,ItemStack report, Player ply, InventoryClickEvent e) {
		if (itemClicked.getType() != Material.AIR) {
			if (itemClicked.getType() == Material.BARRIER) {
				if (e.getSlot() == submit.getSlot()) {
					UUID uuid = getUUID(report);
					OfflinePlayer OfflineReported = Bukkit.getOfflinePlayer(uuid);
					System.out.println(permBan + "ist diess");
					if (!permBan) {
						if (OfflineReported.isOnline()) {
							((Player) OfflineReported).kickPlayer(new PlayerJoinLeaveEvent().getKickMessage(ply, "Permanent"));
						}
						new ManagerReport().setFinished(uuid, ply, -1);
					}
					
					if (permBan) {
						if (OfflineReported.isOnline()) {
							((Player) OfflineReported).kickPlayer(new PlayerJoinLeaveEvent().getKickMessage(ply, time));
						}
						long returnTime = scrapTime(itemStackETListBooks);
						long SystemTime = System.currentTimeMillis();
						
						new ManagerReport().setFinished(uuid, ply, SystemTime + returnTime);
					}
				}
			}
		}
	}


	/**
	 * @param itemStackETList
	 * @return
	 */
	private long scrapTime(List<ItemStackET> itemStackETList) {
		
		long timeTotal = 0;
		
		for (ItemStackET item : itemStackETList) {
			long time = item.getTime();
			long amount = item.getAmount();
			
			timeTotal += time * amount;
		
		}
		
		return timeTotal;
	}


	/**
	 * @param report
	 * @return 
	 */
	private UUID getUUID(ItemStack report) {
		if ((report instanceof ItemStack) && (report != null)) {
			ItemMeta iMetaReport = report.getItemMeta();
			if (iMetaReport.getLore() != null) {
				List<String> lore = iMetaReport.getLore();
				for (String str : lore) {
					if (str.startsWith(getConfigName("Report.uuid"))) {
						String uuidStr = str.substring(uuidLength);
						
						return UUID.fromString(uuidStr);
					}
				}
			}
		}
		return null;	
	}


	/**
	 * @param string
	 * @return
	 */
	private String getConfigName(String string) {
		
		String str = cfg.getString(string);
		str = Reporter.translateUmlaute(str);
		str = ChatColor.translateAlternateColorCodes('&', str);
		return str;
	}

	private void resetlengths() {

		uuidLength 		= inputlength("Report.uuid");
		insultLength 	= inputlength("Report.insult");
		hackingLength 	= inputlength("Report.hacking");
		spamLength 		= inputlength("Report.spam");
		griefingLength 	= inputlength("Report.griefing");
		playerLength 	= inputlength("Report.player");
		allLength 		= inputlength("Report.allreports");
		spacerLength 	= inputlength("Report.spacer");

	}

	private int inputlength(String path) {
		
		String cfgInput = cfg.getString(path);
		return (int) cfgInput.length();

	}

	private void initLengths() {

		uuidLength 		= uuidLength 		+ spacerLength;
		insultLength 	= insultLength 		+ spacerLength;
		hackingLength 	= hackingLength 	+ spacerLength;
		spamLength 		= spamLength 		+ spacerLength;
		griefingLength 	= griefingLength 	+ spacerLength;
		allLength 		= allLength 		+ spacerLength;
		playerLength 	= playerLength 		+ spacerLength;
		
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the permBann
	 */
	public static boolean isPermBann() {
		return InventoryItemsBan.permBan;
	}

	/**
	 * @param permBann the permBann to set
	 */
	public static void setPermBann(boolean permBan) {
		InventoryItemsBan.permBan = permBan;
	}

}
