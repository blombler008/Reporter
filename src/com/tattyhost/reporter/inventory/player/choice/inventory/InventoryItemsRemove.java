/**
 * @author TATTYPLAY
 * @date Jun 1, 2018
 * @created 7:34:10 PM
 * @filename InventoryItemsRemove.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory.player.choice.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.player.choice.inventory;

import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerReport;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.CreationItems;

public class InventoryItemsRemove {
	private static FileConfiguration cfg = Reporter.getInstance().getConfig();
	
	private ManagerReport mr = new ManagerReport();
	
	public InventoryItemsRemove(InventoryClickEvent e, Player ply){
		
		ItemStack item = e.getInventory().getItem(((9 * 3) / 2 ));
		ItemMeta iMeta = item.getItemMeta();
		List<String> iList = iMeta.getLore();
		
		String uuid = iList.get(6);
		String uuidCFG = cfg.getString("Report.uuid");
		String spacerCFG = cfg.getString("Report.spacer");
		
		int uuidCFGLenth = uuidCFG.length();
		int spacerCFGLenth = spacerCFG.length();
		
		UUID puuid = UUID.fromString( uuid.substring( uuidCFGLenth + spacerCFGLenth ));
		mr.delete(puuid);
		
		CreationItems.addItemsAdminInv(CreationInventory.invAdmin.getInventory(), ply);
		ply.openInventory(CreationInventory.invAdmin.getInventory());
	}
	
}
