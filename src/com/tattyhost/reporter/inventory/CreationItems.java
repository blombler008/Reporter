/**

 * @author TATTYPLAY
 * @date May 23, 2018
 * @created 9:02:13 PM
 * @filename CreationItems.java
 * @project Reporter
 * @package com.tattyhost.reporter.inventory
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import com.tattyhost.reporter.ItemStackE;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerMySQL;
import com.tattyhost.reporter.inventory.invs.InventoryAdmin;
import com.tattyhost.reporter.inventory.items.CreateHomeStar;

public class CreationItems {
	public static ItemStackE itemChest = new ItemStackE("�4Reports", Material.CHEST, 0, 1, ((9 * 3) / 2) + 1);

	private static Reporter m = Reporter.getInstance();
	private static ManagerMySQL mysql = m.getMySQL();
	
	public static void addItemsAdminInv(Inventory inv, Player ply) {
		
		mysql.connect(false);
		
		List<String> itemLore = new ArrayList<String>();

		ItemMeta iMeta = itemChest.getItemMeta();

			
		try {
				
			PreparedStatement ps = mysql.getConnection(false).prepareStatement("SELECT * FROM `reports`;");
			ResultSet rs = ps.executeQuery();
			int zaehler = 0;
			while (rs.next()) {
				zaehler++;
			}
				
				
			List<String> supItemList = Reporter.getInstance().getConfig().getStringList("Report.reportslore");
			for(String supItem: supItemList){
				supItem = Reporter.translateUmlaute(supItem);
				supItem = ChatColor.translateAlternateColorCodes('&', supItem);
				
				if (zaehler != 0) {
					supItem = supItem.replaceAll("\\[count\\]", Integer.toString(zaehler));
				} else {
					supItem = supItem.replaceAll("\\[count\\]", Reporter.getInstance().getConfig().getString("Report.noreports"));
				}
				iMeta.setDisplayName(itemChest.getName());
				itemLore.add(supItem);
			}
			
				
			iMeta.setLore(itemLore);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

			
			
			

			itemChest.setItemMeta(iMeta);
		


		inv.setItem(itemChest.getSlot(), itemChest.getItemStack());
	}
	
	public static ItemStackE itemSignNext 		= new ItemStackE("�bNext", Material.SIGN, 0, 1, 9 * 6);
	public static ItemStackE itemSignPrev 		= new ItemStackE("�bPrevios", Material.SIGN, 0, 1, (9 * 6) - 8);
	public static CreateHomeStar itemStar 		= new CreateHomeStar(50);

	public static void addItemsReportsInv(Inventory inv, int page) {
		
		inv.clear();
		inv.setItem(itemStar.getSlot(),itemStar.getItem());
		if (InventoryAdmin.pages == 0) {
			
			
		} else if (InventoryAdmin.currpage == 0) {

			inv.setItem(itemSignNext.getSlot(), itemSignNext.getItemStack());

		} else if (InventoryAdmin.currpage == InventoryAdmin.pages) {

			inv.setItem(itemSignPrev.getSlot(), itemSignPrev.getItemStack());

		} else {

			inv.setItem(itemSignNext.getSlot(), itemSignNext.getItemStack());
			inv.setItem(itemSignPrev.getSlot(), itemSignPrev.getItemStack());

		}

	}

	public static void getPages() {

	}
}
