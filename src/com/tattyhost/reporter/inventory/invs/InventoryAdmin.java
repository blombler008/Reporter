/**
 * @author TATTYPLAY
 * @date May 24, 2018
 * @created 6:52:37 PM
 * @filename InventoryAdmin.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener.invs
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.invs;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerMySQL;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.CreationItems;
import com.tattyhost.reporter.inventory.items.CreationItemsReport;

public class InventoryAdmin {
	private Reporter m = Reporter.getInstance();
	private ManagerMySQL mysql = m.getMySQL();
	
	
	public static int pages = 0;
	public static int currpage = 0;
	public static ResultSet rs;

	public InventoryAdmin(InventoryClickEvent e, Player ply) {

		// for (int i = 0; i < 9 * 6; i++) {
		// ManagerReport.update(UUID.randomUUID(), "System", "spam", false);
		// }
		
		if (e.getCurrentItem() != null) {

			if (e.getCurrentItem().getType() == CreationItems.itemChest.getMaterial()) {

				if (e.getSlot() == CreationItems.itemChest.getSlot()) {

					if (ply.hasPermission("reporter.admin")) {
						
						try {
							
							pages = calcPages();
							if(pages != -1){
								ply.openInventory(CreationInventory.invReports.getInventory());
								CreationItems.itemSignNext.setName("�bNext Page " + (currpage + 1));
								CreationItems.itemSignPrev.setName("�bPrev Page " + (currpage - 1));
								CreationItems.addItemsReportsInv(CreationInventory.invReports.getInventory(), pages );
								new CreationItemsReport().createReportBooks(0, CreationInventory.invReports.getInventory(), true );
							}
							
							
						} catch (SQLException e2) {
							e2.printStackTrace();
						}
						
					}
					
				}
				
			}

		}

		e.setCancelled(true);
	}

	private int calcPages() throws SQLException{
		
		int pages = 0;
		
		PreparedStatement ps = mysql.getConnection(false).prepareStatement("SELECT * FROM `reports`;");
		ResultSet rs = ps.executeQuery();
		
		int cutoff = 0;
	
		while (rs.next()){
			
			if (cutoff == 44) {
				
				cutoff = 0;
				pages++;
				
			} else {
				
				cutoff++;
			}
			
			
			
		}
		if(cutoff == 0){
			
			pages = pages - 1;
			return pages;
			
		}else if(cutoff != 0) {
			
			return pages;
		}
		return -1;
	}
}
