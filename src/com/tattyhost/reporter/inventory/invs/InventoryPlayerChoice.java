/**
 * @author TATTYPLAY
 * @date May 24, 2018
 * @created 6:57:33 PM
 * @filename InventoryPlayerChoice.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener.invs
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.invs;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.tattyhost.reporter.EInventoryMode;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.CreationItems;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;
import com.tattyhost.reporter.inventory.player.choice.inventory.InventoryItemsRemove;
import com.tattyhost.reporter.inventory.player.choice.inventory.InventoryItemsSpring;

public class InventoryPlayerChoice {
	public static EInventoryMode mode = EInventoryMode.NONE;
	
	public InventoryPlayerChoice(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem() != null) {
			switch (mode) {
				case NONE:
					mode = EInventoryMode.NONE;
					exitstar(e,ply);
					
					//itemBan(e, ply);
					itemSpring(e, ply);
					itemWarn(e, ply);
					itemRemove(e, ply);
					
					break;
				case SPRING:
					
					itemSpring(e, ply);
					
					break;
				case WARN:
					
					itemWarn(e, ply);
					
					break;
				case REMOVE:
					
					itemRemove(e, ply);
					
					break;
				//case BANN:
					//mode = EInventoryMode.NONE;
					//itemBan(e, ply);
					//new InventoryItemsBan(e, ply);
					//break;
				default:
					break;
			}
		}
		e.setCancelled(true);
		
		
	}
	
	private void itemSpring(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem().getType() == CreationItemsPlayersChoice.itemSpringWool.getMaterial()) {
			if (e.getSlot() == CreationItemsPlayersChoice.itemSpringWool.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {
					new InventoryItemsSpring(e, ply);
					mode = EInventoryMode.NONE;
				}
			}
		}
	}

	private void itemWarn(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem().getType() == CreationItemsPlayersChoice.itemWarnWool.getMaterial()) {
			if (e.getSlot() == CreationItemsPlayersChoice.itemWarnWool.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {
					mode = EInventoryMode.WARN;
				}
			}
		}
	}

	private void itemRemove(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem().getType() == CreationItemsPlayersChoice.itemRemoveWool.getMaterial()) {
			if (e.getSlot() == CreationItemsPlayersChoice.itemRemoveWool.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {
					new InventoryItemsRemove(e,ply);
					mode = EInventoryMode.REMOVE;
				}
			}
		}
	}

//	private void itemBan(InventoryClickEvent e, Player ply){
//		if (e.getCurrentItem().getType() == CreationItemsPlayersChoice.itemBanWool.getMaterial()) {
//			if (e.getSlot() == CreationItemsPlayersChoice.itemBanWool.getSlot()) {
//				if (ply.hasPermission("reporter.admin")) {
//					new InventoryItemsBan(e, ply);
//					mode = EInventoryMode.BANN;
//				}
//			}
//		}
//	}
	
	private void exitstar(InventoryClickEvent e, Player ply) {
		
			if (e.getCurrentItem().getType() == CreationItemsPlayersChoice.star.getItem().getType()) {
				if (e.getSlot() == CreationItemsPlayersChoice.star.getSlot()) {
					if (ply.hasPermission("reporter.admin")) {
						CreationItems.addItemsAdminInv(CreationInventory.invAdmin.getInventory(), ply);
						ply.openInventory(CreationInventory.invAdmin.getInventory());
						mode = EInventoryMode.NONE;
					}
				}
			}
		
	}
	
	
}
