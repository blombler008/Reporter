/**
 * @author TATTYPLAY
 * @date May 24, 2018
 * @created 6:54:08 PM
 * @filename InventoryReports.java
 * @project Reporter
 * @package com.tattyhost.reporter.listener.inbs
 * @copyright 2018
 */
package com.tattyhost.reporter.inventory.invs;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.CreationItems;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;
import com.tattyhost.reporter.inventory.items.CreationItemsReport;

public class InventoryReports {
	
	public InventoryReports(InventoryClickEvent e, Player ply) {
		
		
		
		if (e.getCurrentItem() != null) {

			signNext(e, ply);

			signPrev(e, ply);
			
			exitstar(e, ply);
			
			if (e.getCurrentItem().getType() == Material.BOOK) {
				if (ply.hasPermission("reporter.admin")) {
					new CreationItemsPlayersChoice().createItemToChoice(e.getCurrentItem(),CreationInventory.invPlayerChoice.getInventory(), ply);
					ply.openInventory(CreationInventory.invPlayerChoice.getInventory());
				}
			}
			
		}
		e.setCancelled(true);
	}
	
	
	private void exitstar(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem().getType() == CreationItems.itemStar.getItem().getType()) {
			if (e.getSlot() == CreationItems.itemStar.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {
					CreationItems.addItemsAdminInv(CreationInventory.invAdmin.getInventory(), ply);
					ply.openInventory(CreationInventory.invAdmin.getInventory());
				}
			}
		}
	}
	
	private void signPrev(InventoryClickEvent e, Player ply) {
		if (e.getCurrentItem().getType() == CreationItems.itemSignPrev.getMaterial()) {
			if (e.getSlot() == CreationItems.itemSignPrev.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {

					InventoryAdmin.currpage -= 1;
					CreationItems.itemSignNext.setName("�bNext Page " + (InventoryAdmin.currpage + 1));
					CreationItems.itemSignPrev.setName("�bPrev Page " + (InventoryAdmin.currpage - 1));
					CreationItems.addItemsReportsInv(CreationInventory.invReports.getInventory(),
							InventoryAdmin.pages);

					new CreationItemsReport().createReportBooks(InventoryAdmin.currpage,
							CreationInventory.invReports.getInventory(),true);
				}
			}
		}
	}
	
	private void signNext(InventoryClickEvent e, Player ply){
		if (e.getCurrentItem().getType() == CreationItems.itemSignNext.getMaterial()) {
			if (e.getSlot() == CreationItems.itemSignNext.getSlot()) {
				if (ply.hasPermission("reporter.admin")) {

					InventoryAdmin.currpage += 1;
					CreationItems.itemSignNext.setName("�bNext Page " + (InventoryAdmin.currpage + 1));
					CreationItems.itemSignPrev.setName("�bPrev Page " + (InventoryAdmin.currpage - 1));
					CreationItems.addItemsReportsInv(CreationInventory.invReports.getInventory(),
							InventoryAdmin.pages);

					new CreationItemsReport().createReportBooks(InventoryAdmin.currpage,
							CreationInventory.invReports.getInventory(), true);
				}
			}
		}
	}
}
