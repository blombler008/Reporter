/**
 * @author TATTYPLAY
 * @date Jul 3, 2018
 * @created 12:33:38 AM
 * @filename ItemStackET.java
 * @project Reporter
 * @package com.tattyhost.reporter
 * @copyright 2018
 */
package com.tattyhost.reporter;

import org.bukkit.Material;

/**
 * 
 */
public class ItemStackET extends ItemStackE{

	private EItemsBanTimeUnits time;
	
	/**
	 * @param name
	 * @param material
	 * @param metaDataID
	 * @param amount
	 * @param slot
	 * @param time 
	 */
	public ItemStackET(String name, int material, int metaDataID, int amount, int slot, EItemsBanTimeUnits time) {
		super(name, material, metaDataID, amount, slot);
		this.time = time;
	}

	/**
	 * @param name
	 * @param material
	 * @param metaDataID
	 * @param amount
	 * @param slot
	 * @param time 
	 */
	public ItemStackET(String name, Material material, int metaDataID, int amount, int slot, EItemsBanTimeUnits time) {
		super(name, material, metaDataID, amount, slot);
		this.time = time;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time.getTime();
	}

	/**
	 * @return
	 */
	public EItemsBanTimeUnits getTimeUnit() {
		return time;
	}
	
}
