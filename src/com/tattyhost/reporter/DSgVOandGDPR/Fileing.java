/**
 * @author TATTYPLAY
 * @date Jun 15, 2018
 * @created 1:41:03 PM
 * @filename Fileing.java
 * @project Reporter
 * @package com.tattyhost.reporter.de.DSgVO
 * @copyright 2018
 */
package com.tattyhost.reporter.DSgVOandGDPR;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import com.tattyhost.reporter.Reporter;

public class Fileing {
	public Fileing(Reporter m) throws IOException{
		
		File file1 = new File("Reporter-DSgVO.txt");
		File file2 = new File("Reporter-GDPR.txt");
		
		if (!file1.exists()){
			if (!file2.exists()){
				file2.createNewFile();
				m.getManagerConsole().out("&5Read the GDPR First");
				readerGDPR(m);
			}
			file1.createNewFile();
			m.starting = false;
			m.getManagerConsole().out("&5Read the DSgVO First");
			m.onDisable();
			readerDSgVO(m);
		} else if (!file2.exists()){
			file2.createNewFile();
			m.starting = false;
			m.getManagerConsole().out("&5Read the GDPR First");
			m.onDisable();
			readerGDPR(m);
		}
		
	}
	
	
	private void readerDSgVO(Reporter m) throws IOException {
		
		InputStream DSgVO_Stream = m.getResource("DSgVO");
		BufferedReader DSgVO_Stream_read = new BufferedReader(new InputStreamReader(DSgVO_Stream, StandardCharsets.UTF_8));
		BufferedWriter writer = new BufferedWriter(new FileWriter("Reporter-DSgVO.txt"));

		String line = DSgVO_Stream_read.readLine();
	    while (line != null) {
	        
	        writer.write(line +"\n");
	        line = DSgVO_Stream_read.readLine();
	    }
		writer.close();
		
	}
private void readerGDPR(Reporter m) throws IOException {
		
		InputStream DSgVO_Stream = m.getResource("GDPR");
		BufferedReader DSgVO_Stream_read = new BufferedReader(new InputStreamReader(DSgVO_Stream, StandardCharsets.UTF_8));
		BufferedWriter writer = new BufferedWriter(new FileWriter("Reporter-GDPR.txt"));

		String line = DSgVO_Stream_read.readLine();
	    while (line != null) {
	        
	        writer.write(line +"\n");
	        line = DSgVO_Stream_read.readLine();
	    }
		writer.close();
		
	}
}
