/**
 * @author TATTYPLAY
 * @date May 19, 2018
 * @created 9:23:02 PM
 * @filename CommandReport.java
 * @project Reporter
 * @package com.tattyhost.reporter.commands
 * @copyright 2018
 */

package com.tattyhost.reporter.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;

import com.tattyhost.reporter.IVersionHandler;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerReport;

import net.md_5.bungee.api.ChatColor;

public class CommandReport implements CommandExecutor, TabCompleter {
	private static String[] resonsReport = { "Hacking", "Griefing", "Spam", "Insult" };
	private HashMap<String, Long> cooldowns = new HashMap<String, Long>();

	private Reporter reporter = Reporter.getInstance();
	private IVersionHandler version = reporter.getVersion();
	
	private ManagerReport mr = reporter.getReportManager();
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (args.length == 1 ) {
			if (args[0].equalsIgnoreCase("version") || args[0].equalsIgnoreCase("-v")) {
				String out = String.format(ChatColor.RED + "Reporter Version: %s", Reporter.getInstance().getDescription().getVersion());
				sender.sendMessage(out);
				return true;
			}
		}
		
		if (sender instanceof Player) {

			Player reporterPlayer = (Player) sender;

			if (args.length >= 2) {

				String identRegistry = "";

				if (Bukkit.getPlayerExact(args[0]) != null) {

					Player reportedPlayer = Bukkit.getPlayerExact(args[0]);
					for (int i = 0; i < resonsReport.length; i++) {
						if (args[1].equalsIgnoreCase(resonsReport[i].intern())) {

							int cooldownTime = Reporter.getInstance().getConfig().getInt("Report.cooldown");
							if (cooldowns.containsKey(sender.getName())) {
								long secondsLeft = ((cooldowns.get(sender.getName()) / 1000) + cooldownTime) - (System.currentTimeMillis() / 1000);
								if (secondsLeft > 0) {
									List<String> outStrings = Reporter.getInstance().getConfig().getStringList("Report.cooldownmsgs");
									for(String outString: outStrings) {
										
										outString = Reporter.translateUmlaute(outString);
										outString = ChatColor.translateAlternateColorCodes('&', outString);
										
										outString = outString.replaceAll("\\[timeleft\\]", ""+secondsLeft);
										outString = outString.replaceAll("\\[user\\]", reportedPlayer.getName());
										
										reporterPlayer.sendMessage(outString);
			
									}
									return true;
								}
							}
							
							cooldowns.put(sender.getName(), System.currentTimeMillis());
							
							identRegistry = args[1].toLowerCase();
							
							String reasonString = Reporter.getInstance().getConfig().getString("Report." + identRegistry.toLowerCase());
							
							reasonString = Reporter.translateUmlaute(reasonString);
							reasonString = ChatColor.translateAlternateColorCodes('&', reasonString);
							
							List<String> reasonOutPutStringList = Reporter.getInstance().getConfig().getStringList("Report.reportmsgs");
							for(String reasonOutPutString: reasonOutPutStringList) {
								
								reasonOutPutString = Reporter.translateUmlaute(reasonOutPutString);
								reasonOutPutString = ChatColor.translateAlternateColorCodes('&', reasonOutPutString);
								
								reasonOutPutString = reasonOutPutString.replaceAll("\\[reason\\]", reasonString);
								reasonOutPutString = reasonOutPutString.replaceAll("\\[user\\]", reportedPlayer.getName());
								
								reporterPlayer.sendMessage(reasonOutPutString);
	
							}
							if (!mr.isUserExists(reportedPlayer.getUniqueId()) ) {
								for (Player player : Bukkit.getOnlinePlayers()) {
									if (!(player.getName().contains(reporterPlayer.getName()) || player.getName().contains(reportedPlayer.getName()))) {
										if (player.hasPermission("reporter.admin")) {
											List<String> adminReportMsgs = Reporter.getInstance().getConfig().getStringList("Report.adminreportmsgs");
											for(String adminMsg: adminReportMsgs){
												
												String adminChangeClickName = Reporter.getInstance().getConfig().getString("Report.adminreportclickchanger");
												
												adminChangeClickName = ChatColor.translateAlternateColorCodes('&', adminChangeClickName);
												adminChangeClickName = Reporter.translateUmlaute(adminChangeClickName);
												
												adminMsg = ChatColor.translateAlternateColorCodes('&', adminMsg);
												adminMsg = Reporter.translateUmlaute(adminMsg);
												adminMsg = adminMsg.replaceAll("\\[user\\]", reportedPlayer.getName());
												adminMsg = adminMsg.replaceAll("\\[reason\\]", reasonString);
												String words [] = adminMsg.split(" ");
	
												int lengthWord = words.length;
												String newMessage = "";
												
												for (int j = 0; j<words.length; j++) {
													
													String word = JSONObject.escape(words[j]);
													word = word.replaceAll("\\[click\\]", adminChangeClickName);
													
													if (!(j == (lengthWord-1))) {
														
														if (word.contains(adminChangeClickName)) {
															newMessage += "\"}, {\"text\":\"" + word + " \",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/reportmenu " + reportedPlayer.getUniqueId().toString() + "\"}}, {\"text\":\"";
														} else {
															newMessage += word + " ";													
														}
														
													} else {
														
														if (word.contains(adminChangeClickName)) {
															newMessage += "\"}, {\"text\":\"" + word + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/reportmenu " + reportedPlayer.getUniqueId().toString() + "\"}}, {\"text\":\"";
														} else {
															newMessage += word;
														}
														
													}
												}
												
												String message = ",{\"text\":\"" + newMessage + "\"";
												message += "}";
												try {
													version.sendToPlayers(message, player, (Player) sender);
												} catch (Exception e) {
													// TODO: handle exception
												}
												
												//player.sendMessage(adminMsg);
												
											}
											
										}
									}
								}
							}
							if(Reporter.getInstance().getConfig().getBoolean("Report.sendmessageafterreport")){
								List<String> reportedMsgs = Reporter.getInstance().getConfig().getStringList("Report.reportedmsgs");
								for (String reportedMsg : reportedMsgs){
									reportedMsg = Reporter.translateUmlaute(reportedMsg);
									reportedMsg = ChatColor.translateAlternateColorCodes('&', reportedMsg);
									
									reportedMsg = reportedMsg.replaceAll("\\[reason\\]", reasonString);
									
									reportedPlayer.sendMessage(reportedMsg);
								}
							}
							

							mr.update(reportedPlayer.getUniqueId(), reportedPlayer.getName(), identRegistry.toLowerCase(), false);
							return true;
						}
					}
					String usage = "�cUsage: /" + command.getName()
							+ " <Spieler> <Grund:[Hacking, Griefing, Spam, Insult] >";
					reporterPlayer.sendMessage(usage);

				} else
					reporterPlayer.sendMessage("�cDer Spieler " + args[0] + " existiert es nicht!");

			} else {
				String usage = "�cUsage: /" + command.getName()
						+ " <Spieler> <Grund:[Hacking, Griefing, Spam, Insult] >";
				reporterPlayer.sendMessage(usage);
			}
		} else
			sender.sendMessage("Du bist kein Spieler!");

		return true;

	}

	/*
	 * @see org.bukkit.command.TabCompleter#onTabComplete(org.bukkit.command.CommandSender, org.bukkit.command.Command, java.lang.String, java.lang.String[])
	 */
	@Override
	public List<String> onTabComplete(CommandSender cmd, Command arg1, String arg2, String[] args) {
		List<String> ret = new ArrayList<String>();
		if (args.length == 2) {
			for (String rs: resonsReport) {
				ret.add(rs);
			}
			return ret;
		}
		return null;
	}
}
