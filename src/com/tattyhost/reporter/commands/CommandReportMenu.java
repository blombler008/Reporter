/**
 * @author TATTYPLAY
 * @date May 22, 2018
 * @created 12:46:03 AM
 * @filename CommandReportMenu.java
 * @project Reporter
 * @package com.tattyhost.reporter.commands
 * @copyright 2018
 */
package com.tattyhost.reporter.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.tattyhost.reporter.EItemsBanTimeUnits;
import com.tattyhost.reporter.Reporter;
import com.tattyhost.reporter.Manager.ManagerBan;
import com.tattyhost.reporter.Manager.ManagerReport;
import com.tattyhost.reporter.inventory.CreationInventory;
import com.tattyhost.reporter.inventory.CreationItems;
import com.tattyhost.reporter.inventory.items.CreationItemsPlayersChoice;
import com.tattyhost.reporter.inventory.items.CreationItemsReport;
import com.tattyhost.reporter.listener.PlayerJoinLeaveEvent;

public class CommandReportMenu implements CommandExecutor {

	private Reporter reporter = Reporter.getInstance();
	private ManagerReport mr = reporter.getReportManager();
	private ManagerBan mb = new ManagerBan(reporter.getMySQL());

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command comand, String label, String[] args) {
		if (sender instanceof Player) {
			Player ply = (Player) sender;

			if (ply.hasPermission("reporter.admin")) {

				if (!(args.length == 0)) {

					if (args[0] != null) {

						if (args[0].equalsIgnoreCase("unban")) {

							try {

								if (args[1] != null) {

									OfflinePlayer offPly = null;
									if (offPly == null)
										if (Bukkit.getOfflinePlayer(args[1]) != null) {
											offPly = Bukkit.getOfflinePlayer(args[1]);

										} else
											ply.sendMessage("�cInvailed Player Name: Check for entering it compleatly");

									if (offPly == null)
										if (Bukkit.getOfflinePlayer(UUID.fromString(args[1])) != null) {
											offPly = Bukkit.getOfflinePlayer(UUID.fromString(args[1]));

										} else
											ply.sendMessage("�cInvailed UUID: Check for entering it compleatly");

									if (offPly != null) {

										mb.setUniqueId(offPly.getUniqueId());

										if (mb.isUserExist())
											mb.removeEntry(true);

									} else
										return true;

								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						} else if (args[0].equalsIgnoreCase("ban")) {

							if (args.length >= 2) {

								try {

									long time = 0;
									String timeUnit = args[2];
									EItemsBanTimeUnits mul;

									try {
										if (mr.isUserExists(Bukkit.getOfflinePlayer(args[1]).getUniqueId())) {

											try {

												try {

													if (timeUnit.endsWith("w")) {
														mul = EItemsBanTimeUnits.WEEKS;
														time = Long.parseLong(
																timeUnit.substring(0, timeUnit.length() - 1));

													} else if (timeUnit.endsWith("d")) {
														mul = EItemsBanTimeUnits.DAYS;
														time = Long.parseLong(
																timeUnit.substring(0, timeUnit.length() - 1));

													} else if (timeUnit.endsWith("h")) {
														mul = EItemsBanTimeUnits.HOURS;
														time = Long.parseLong(
																timeUnit.substring(0, timeUnit.length() - 1));

													} else if (timeUnit.endsWith("m")) {
														mul = EItemsBanTimeUnits.MINUTS;
														time = Long.parseLong(
																timeUnit.substring(0, timeUnit.length() - 1));

													} else if (timeUnit.endsWith("s")) {
														mul = EItemsBanTimeUnits.SECOUNDS;
														time = Long.parseLong(
																timeUnit.substring(0, timeUnit.length() - 1));

													} else {
														mul = EItemsBanTimeUnits.SECOUNDS;

														time = Long.parseLong(timeUnit);
													}

												} catch (Exception e) {
													ply.sendMessage(
															"�cInvailed Arguments: Check for entering it compleatly!");
													return true;
												}
											} catch (Exception e) {

												ply.sendMessage(
														"�cInvailed Arguments: Check for entering it compleatly!");
												return true;
											}

											long timeBan = (time * mul.getTime());
											long SystemTime = (System.currentTimeMillis() / 1000);

											mr.setFinished(Bukkit.getOfflinePlayer(args[1]).getUniqueId(), ply,
													(SystemTime + timeBan) * 1000);
											try {
												OfflinePlayer plyToBan = Bukkit.getOfflinePlayer(args[1]);
												if (plyToBan.isOnline()) {
													((Player) plyToBan).kickPlayer(new PlayerJoinLeaveEvent()
															.getKickMessage(ply, mb.getRemainingTime(timeBan)));
												}
											} catch (Exception e) {

											}

										} else {
											ply.sendMessage("�cUser had not been reportet jet");
											return true;
										}

									} catch (Exception e) {
										ply.sendMessage("�cInvailed Arguments: Check for entering it compleatly!");
										return true;
									}
								} catch (Exception e) {

									ply.sendMessage("�cInvailed Arguments: Check for entering it compleatly!");
									return true;
								}

							} else
								ply.sendMessage("�cInvailed Arguments: Check for entering it compleatly!");

						} else if (mr.isUserExists(Bukkit.getOfflinePlayer(args[1]).getUniqueId())) {

							ItemStack reportBook = new CreationItemsReport().createReportBook(false,
									UUID.fromString(args[0]));

							new CreationItemsPlayersChoice().createItemToChoice(reportBook,
									CreationInventory.invPlayerChoice.getInventory(), ply);

							ply.openInventory(CreationInventory.invPlayerChoice.getInventory());

							return true;

						}
					}

				} else if (args.length == 0) {
					CreationItems.addItemsAdminInv(CreationInventory.invAdmin.getInventory(), ply);
					ply.openInventory(CreationInventory.invAdmin.getInventory());
				}
			}
		} else {
			sender.sendMessage("Du bist Kein Spieler!");
		}
		return true;
	}

}
